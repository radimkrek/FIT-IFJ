/* ******************************* scanner.h ******************************** */
/* Soubor:              scanner.h - Hlavickovy soubor pro                     */
/*                                    Lexikalni analyzator (scanner)          */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                   -> Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#ifndef SCANNER_H_INCLUDED
#define SCANNER_H_INCLUDED

#include "constants.h"
#include "str.h"

// sdílené proměnné - aktuálně načtěný řádek a znak ve zdrojovém souboru
extern int errRow;  // radek chybneho znaku
extern int errChar; // pozice chybneho znaku

/**
* Nastavi zdrojovy soubor
* @param  *f  Ukazatel na zdrojovy soubor
*/
void setSourceFile(FILE *f);

/**
* Nastavi jmeno souboru
* @param  *f  Ukazatel na zdrojovy soubor
*/
void setFileName(char *f);

/**
* Vrati dalsi token
*  -> informace o automatu v grafu FSM
* @param  *token     Ukazatel na token
* @return  int >= 0  Cislo odpovidajici typu tokenu
* @return  -1        Lexikalni chyba
* @return  -99       Interni chyba
*/

#endif /*SCANNER_H_INCLUDED*/
