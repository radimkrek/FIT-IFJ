/* **************************** main.c ************************************** */
/* Soubor:              main.c - Ridici funkce interpretu                     */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                      Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */


#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "parser.h"
#include "ial.h"

/**
 * Makro inicializuje pouzivana rozhrani:
 *   -soubor se zdrojovym kodem
 *   -strukturu pro token
 */
#define RES_INIT       \
  setSourceFile(file); \
  setFileName(argv[1]);\
                       \
  string token;        \
  strInit(&token);

/**
 * Makro uvolni pouzivana rozhrani:
 *   -soubor se zdrojovym kodem
 *   -strukturu pro token
 */
#define RES_FREE      \
  fclose(file);       \
                      \
  strFree(&token);

int main(int argc, char * argv[])
{
  FILE *file;
  if (argc != 2){
    fprintf(stderr, "Chyba parametru prikazove radky\n");
    return EINT;
  }
  if ((file = fopen(argv[1], "r")) == NULL){
    fprintf(stderr, "Chyba pri otevirani souboru\n");
    return EINT;
  }
  RES_INIT // inicializace zdroju


  // volani parseru, v pripade chyby vraci jeji kod
  // parser sám také volá po svém skončení interpret
  int parserResult;
  parserResult = callParser();
  // printf("\nPARSER RESULT: %d\n\n", parserResult);


  RES_FREE // uvolneni zdroju
  return parserResult;
}
