/* ***************************** instructions.h ***************************** */
/* Soubor:              instructions.h - Hlavickovy soubor pro                */
/*                                         Zretezeny seznam instrukci         */
/* Kodovani:            UTF-8                                                 */
/* Datum:               listopad.2014                                         */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                   -> Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */
#ifndef INSTRUCTIONS_H_INCLUDED
#define INSTRUCTIONS_H_INCLUDED

#include "ial.h"
#include "str.h"

#define INST_ERROR   1
#define INST_SUCCESS 0

/* *********************** Zretezeny seznam instrukci *********************** */
typedef struct  // struktura pro instrukci
{
  int instType; // typ instrukce
  void *addr1;  // adresa 1
  void *addr2;  // adresa 2
  void *addr3;  // adresa 3
} tInstr;

typedef struct listItem      // struktura pro jeden clanek seznamu
{
  tInstr Instruction;        // aktualni instrukce
  struct listItem *nextItem; // ukazatel na dalsi instrukci
} tListItem;

typedef struct               // hlavicka zretezeneho seznamu
{
  struct listItem *first;    // ukazatel na prvni instrukci
  struct listItem *last;     // ukazatel na posledni instrukci
  struct listItem *active;   // ukazatel na aktivni instrukci
} tListOfInstr;

void listInit(tListOfInstr *L);       // inicializuje seznam instrukci
void listFree(tListOfInstr *L);       // uvolni seznam instrukci
void listInsertLast(tListOfInstr *L, tInstr I);  // vlozi instrukci na konec seznamu
void listFirst(tListOfInstr *L);      // nastavi active na prvni instrukci
void listNext(tListOfInstr *L);       // nastavi active na dalsi instrukci
void listGoto(tListOfInstr *L, void *gotoInstr); // nastavi active na pozadovanou instrukci
tInstr *listGetData(tListOfInstr *L); // vrati ukazatel na aktivni instrukci

/* ***************************** Zasobnik ramcu ***************************** */
typedef struct stackItem  // struktura pro polozku zasobniku
{
  table *addr;            // ukazatel na ramec (zkopirovana lokalni TS)
  struct stackItem *next; // ukazatel na dalsi polozku
} tStackItem;

typedef struct            // hlavicka zasobniku
{
  struct stackItem *top;  // ukazatel na vrchol zasobniku
} tStack;

void instStackInit(tStack *stack); // inicializuje zasobnik
void instStackFree(tStack *stack); // uvolni zasobnik
int  push(tStack *stack, table *addr); // vlozi ramec na vrchol zasobniku
void itemFree(tStackItem *item);   // uvolni ramec (jiz odstraneny ze zasobniku)
tStackItem *pop(tStack *stack);    // odstrani ramec ze zasobniku (neuvolnuje)
table *top(tStack *stack);         // vrati ukazatel na ramec na vrcholu zasobniku
bool empty(tStack *stack);         // otestuje jestli je zasobnik prazdny

/* *********** Datové struktury pro práci se zásobníkem instrukcí *********** */
typedef struct instructStackItem{ // položka zásobníku instrukcí
  tListItem *addr;                // ukazatel na instrukci
  struct instructStackItem *next; // ukazatel na následující položku zásobníku
} tInstructStackItem;

typedef struct{                   // zásobník instrukcí ve formě zřetězeného seznamu
  struct instructStackItem *top;  // ukazatel na vrchol zasobniku
} tInstructStack;

/* **************** Funkce pro práci se zásobníkem instrukcí **************** */

/**
* Inicializuje zásobník instrukcí
* @param stack Ukazatel na zásobník instrukcí
* @return void
*/
void instructStackInit(tInstructStack *stack);

/**
* Uvolní zásobník instrukcí
* @param stack Ukazatel na zásobník instrukcí
* @return void
*/
void instructStackFree(tInstructStack *stack);

/**
* Vloží instrukci na vrchol zásobníku
* @param stack Ukazatel na zásobník instrukcí
* @param stack Ukazatel na vkládanou instrukci
* @return int INST_SUCCESS v případě uspěchu a INST_ERROR v případě neúspěchu
*/
int  instructPush(tInstructStack *stack, tListItem *addr);

/**
* Odebere instrukci z vrcholu zásobníku a vrátí ji
* @param stack Ukazatel na zásobník instrukcí
* @return instructPop Ukazatel na instrukci, kterou jsme odebrali z vrcholu zásobníku
*/
tListItem *instructPop(tInstructStack *stack);

/**
* OPřečte instrukci z vrcholu zásobníku
* @param stack Ukazatel na zásobník instrukcí
* @return instrucTop Ukazatel na instrukci, kterou jsme přečetli
*/
tListItem *instructTop(tInstructStack *stack);

/**
* Funkce zjistí, jestli je zásobník prázdný
* @param stack Ukazatel na zásobník instrukcí
* @return bool
*/
bool instructEmpty(tInstructStack *stack);

#endif /*INSTRUCTIONS_H_INCLUDED*/
