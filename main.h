/* **************************** main.h ************************************** */
/* Soubor:              main.h - Hlavickovy soubor pro                        */
/*                                 Ridici funkce interpretu                   */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                      Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>

#include "scanner.h"

/** Kody chyb programu */
enum tecodes
{
  EOK     = 0,    // < Bez chyby
  ESCAN   = 1,    // < Lexikalni chyba
  EPARS   = 2,    // < Syntakticka chyba
  ESEM_D  = 3,    // < Semanticka chyba - unDefined/reDefinition
  ESEM_M  = 4,    // < Semanticka chyba - type Mismatch
  ESEM_E  = 5,    // < Semanticka chyba - Else
  ERUN_N  = 6,    // < Behova chyba - chyba nacteni cisla ze vstupu
  ERUN_U  = 7,    // < Behova chyba - Uninicialized variable
  ERUN_D  = 8,    // < Behova chyba - Division by zero
  ERUN_E  = 9,    // < Behova chyba - Else
  EINT    = 99,   // < Internal error
};

#endif // MAIN_H_INCLUDED
