#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>

// struktura polozky seznamu
struct gc_item {
	int* value;
	struct gc_item *next;
};

// vrchol seznamu
struct gc_item *gc_head = NULL;

/**
* Prida hodnotu (pointer) do seznamnu alokovane pameti
* @param *value pointer alokovane pameti 
*/
struct gc_item* gc_add(int* value) {
	struct gc_item *item;

	// alokovani pameti pro poloku seznamu
	item = (struct gc_item*)malloc(sizeof(struct gc_item));

	if (item == NULL) {
		// nedostatek pameti, nelze alokovat pamet pro polozku seznamu
		return NULL;
	}
	
	// do polozky se zapise ukladana hodnota a odkaz na zacatek seznamu (na nejz vkladame tuto novou)
	item->value = value;
	item->next = gc_head;

	// vlozeni polozky na zacatek seznamu
	gc_head = item;
	return item;
}

/**
* Najde hodnotu (pointer) v seznamnu alokovane pameti
* @param *value pointer alokovane pameti
* @param **prev odkaz na polozku v seznamu, ktera predchazi nalezene polozce
*/
struct gc_item* gc_find(int* value, struct gc_item **prev) {
	struct gc_item *item = gc_head;
	struct gc_item *tmp = NULL;
	bool found = false;

	while (item != NULL) {
		// pruchod celym seznamem, dokud se nedojde na konec
		if (item->value == value) {
			// pokud je nalezen hledany zaznam, priznak found se naastavi na true a prerusi cyklus
			found = true;
			break;
		} else {
			// pokud se nejedna o hledany zaznam, polozka se poznaci jako predchazejici
			tmp = item;
			// a jde se prozkoumat dalsi polozka seznamu
			item = item->next;
		}
	}

	if (found == true) {
		// pokud byla polozka v seznamu nalezena, vratime ji a zapiseme predchozi do *prev
		if (prev) {
			*prev = tmp;
		}
		return item;
	} else {
		// pokud polozka nebyla nalezena
		return NULL;
	}
}

/**
* Odstrani hodnotu (pointer) ze seznamnu alokovane pameti
* @param *value pointer alokovane pameti
*/
int gc_remove(int* value) {
	struct gc_item *prev = NULL;
	struct gc_item *del = NULL;

	// hledani polozky v seznamu
	del = gc_find(value, &prev);

	if (NULL == del) {
		// polozka nalezena
		return -1;
	} else {
		// pokud je polozka nalezena

		if (prev != NULL) {
			// pokud neni mazana polozka prvni v seznamu, spoji odkaz mezi predchozi a nasledujici
			prev->next = del->next;
		} else if (del == gc_head) {
			// pokud je mazana polozka na zacatku seznamnu, presune na nej nasledujici polozku
			gc_head = del->next;
		}

		// uvolneni pameti polozky
		free(del);
		del = NULL;

		// navratova hodnota pro ok - polozka nalezena a smazana
		return 0;
	}
}

void print_list(void) {
	struct gc_item *ptr = gc_head;

	printf("\n-------Printing list Start-------\n");
	while (ptr != NULL) {
		printf("[%p] \n",ptr->value);
		ptr = ptr->next;
	}
	printf("-------Printing list End-------\n\n");

	return;
}

/**
* Alokuje pamet o danne velikosti, vrati pointer a zaroven ho ulozi do seznamu alokovane pameti
* @param *size velikost alokovane pameti
*/
int * memalloc(int size) {
	int *pointer;
	// alokovani pameti o danne velikosti
	pointer = malloc(size);
	// ulozeni adresy pointeru do seznamu veskere alokovane pameti
	gc_add(pointer);
	// navrat pointeru alokovane pameti uzivateli
	return pointer;
}

/**
* Realokuje dannou pamet o danne velikosti, vrati pointer a zaroven ho ulozi do seznamu alokovane pameti
* @param *pointer pointer na pamet ktera ma byt realokovana
* @param *size velikost alokovane pameti
*/
int * memrealloc(int * pointer, int size) {
	struct gc_item *prev = NULL;
	struct gc_item *item = NULL;

	// hledani polozky (aktualne alokovane pameti) v seznamu
	item = gc_find(pointer, &prev);
	// prepsani puvodniho pointeru za nove alokovany
	pointer = realloc(pointer, size);
	// prepsani pointeru v seznamu
	item->value = pointer;
	// navrat pointeru alokovane pameti uzivateli
	return pointer;
}

/**
* Uvolni pamet dannou pointerem a odstrani jej ze seznamu alokovane pameti
* @param *pointer pointer na pamet ktera ma byt uvolnena
*/
void memfree(int * pointer) {
	int ret;
	// odstraneni polozky (pointer na pamet kterou chceme uvolnit) ze seznamu
	ret = gc_remove(pointer);
	if (ret == 0) {
		// polozka byla nalezena a odstranena ze seznamu, ted muzeme pointer uklidit
		free(pointer);
	}
}

/**
* Uvolni veskerou alokovanou pamet, ktera nebyla drive uvolnena
*/
void memrelease() {
	struct gc_item *ptr = gc_head;
	// pruchod seznamem pokud jsou v nem polozky
	while (ptr != NULL) {
		// docasne uschovani mazane hodnoty do promenne temp
		int * temp = ptr->value;
		// dosazeni aktualni polozky do promenne ptr
		ptr = ptr->next;
		// nyni muzeme bezpecne smazat polozku aniz by se rozbil cyklus
		memfree(temp);
	}
}




int main (void) {
	int *pointer1;
	int *pointer2;
	int *pointer3;
	int *pointer4;
	int *pointer5;

	pointer1 = memalloc(1);
	pointer2 = memalloc(890);
	pointer3 = memalloc(2000);

	print_list();
	memfree(pointer1);
	print_list();
	memfree(pointer2);
	print_list();
	memfree(pointer3);
	print_list();
	pointer4 = memalloc(2000);
	pointer4 = memrealloc(pointer4, 1);
	print_list();
	pointer5 = memrealloc(pointer4, 9999);
	print_list();
	memrelease();
	print_list();

	return 0;
}
