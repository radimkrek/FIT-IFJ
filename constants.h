#ifndef CONSTANTS_H_INCLUDED
#define CONSTANTS_H_INCLUDED

/* **************************** Instrukcni sada ***************************** */
// Instrukce generované parserem (nikoliv výrazy)

#define I_HALT    0  // EOF
#define I_NOP 1 // NOP - nedelej nic, ale bud v listu instrukci

#define I_CREATE_FRAME   3  // (adresa_do_globalni_TS_na_funkci, NULL, NULL)
#define I_CALL       4  // call(adresa_do_globalni_TS_na_funkci, NULL, NULL)
#define I_CALL_VEST   5  // call(adresa_do_globalni_TS_na_funkci, NULL, adresa_pro_vysledek)
#define I_SET_PAR_VEST   7  // call(adresa_do_TS_kde_bude_nazev_promenne, NULL, NULL)
#define I_SET_PAR    6  // (adresa_do_TS_kde_bude_nazev_promenne, NULL, adresa_do_lokalni_tabulky_symbolu_na_nazev_parametru)
#define I_RETURN    12 // return(NULL, NULL, NULL) pokyn pro interpret pro navret z fce
#define I_SET_RESULT_AND_DESTROY_FRAME    11 // return(NULL, NULL, adresa_do_TS_kam_vratit_vysledek) pokyn pro interpret aby uložil výsledek funkce odstranil rámec z vrcholu zásobníku

#define I_ASSIGN 13 // (UKHodnotyPraveStrany, NULL, UKvysledek)
#define I_IF     14 // (UKVysledekVyrazu, NULL, UkNaPrvniInstrukciZaElse) Když je vysledek vyrazu true pokracuje se nasledujici instrukci,
                 // jinak se skočí na vykonávání instrukce UkNaElse
#define I_ELSE           15 // (NULL, NULL, UkNaKonecBlokuElse) // pokud se if vykonal, else se preskoci
#define I_WHILE          16 // (UKVysledekVyrazu, NULL, UkNaPrvniInstrukciZaInstrukciI_REPEAT_WHILE)
#define I_REPEAT_WHILE   17 // (NULL, NULL, UkNaVyhodnoceniVyrazuVPodminceWhile)
#define I_READLN         18 // (NULL, NULL, UkNaNactenouPromennou)
#define I_WRITE          19 // (UkNaJmeno_literalu_nebo_promenne, NULL, NULL) Vypíšeme literál

// Instrukce pro výrazy: Aritmetické, řetězcové a relační operátory
#define I_ADD           21 // (UkNaOp1, UkNaOp2, UkNaVysledek) NUMBER + NUMBER
#define I_STR_CONCAT    22 // (UkNaOp1, UkNaOp2, UkNaVysledek) STRING . STRING
#define I_SUB           23 // (UkNaOp1, UkNaOp2, UkNaVysledek) NUMBER - NUMBER
#define I_MUL           24 // (UkNaOp1, UkNaOp2, UkNaVysledek) NUMBER * NUMBER
#define I_DIV           25 // (UkNaOp1, UkNaOp2, UkNaVysledek) NUMBER / NUMBER
#define I_LESS          26 // (UkNaOp1, UkNaOp2, UkNaVysledek) NUMBER < NUMBER
#define I_MORE          27 // (UkNaOp1, UkNaOp2, UkNaVysledek) NUMBER > NUMBER
#define I_LESS_EQUAL    28 // (UkNaOp1, UkNaOp2, UkNaVysledek) NUMBER <= NUMBER
#define I_MORE_EQUAL    29 // (UkNaOp1, UkNaOp2, UkNaVysledek) NUMBER >= NUMBER
#define I_EQUAL         30 // (UkNaOp1, UkNaOp2, UkNaVysledek) NUMBER = NUMBER
#define I_LESS_MORE     31 // (UkNaOp1, UkNaOp2, UkNaVysledek) NUMBER <> NUMBER

/* ****************************** Typy tokenu ******************************* */
// identifikator
#define ID           0

// literaly
#define LIT_INTEGER  1  // cisla typu integer
#define LIT_REAL     2  // cisla typu real
#define LIT_STRING   3  // retezce

// klicova slova
#define KW_BEGIN     10
#define KW_END       11
#define KW_VAR       12
#define KW_IF        13
#define KW_THEN      14
#define KW_ELSE      15
#define KW_DO        16
#define KW_WHILE     17
#define KW_TRUE      18
#define KW_FALSE     19
#define KW_FUNCTION  20
#define KW_FORWARD   21
#define KW_READLN    22
#define KW_WRITE     23
#define KW_SORT      24
#define KW_FIND      25

// datove typy
#define DT_INTEGER   30
#define DT_REAL      31
#define DT_STRING    32
#define DT_BOOLEAN   33

// operatory
#define OP_ASSIGN        40 // :=
#define OP_ADD           41 // +
#define OP_SUB           42 // -
#define OP_MUL           43 // *
#define OP_DIV           44 // /
#define OP_LESS          45 // <
#define OP_MORE          46 // >
#define OP_LESS_EQUAL    47 // <=
#define OP_MORE_EQUAL    48 // >=
#define OP_EQUAL         49 // =
#define OP_LESS_MORE     50 // <>

// jednotlive znaky
#define COLON            60 // :
#define SEMICOLON        61 // ;
#define BRACKET_L        62 // (
#define BRACKET_R        63 // )
#define DOT              64 // .
#define COMA             65 // ,
#define END              66 // End Of File

// rozdeleni Funkce x promenna
#define FUNCTION         67
#define VARIABLE         68

// chyba
#define SCAN_ERROR       -1   // Lexikalni chyba
#define RUN_N_ERROR       6   // Behova chyba - chyba nacteni cisla ze vstupu
#define RUN_U_ERROR       7   // Behova chyba - Uninicialized variable
#define RUN_D_ERROR       8   // Behova chyba - Division by zero
#define RUN_E_ERROR       9   // Behova chyba - Else
#define INTERN_ERROR      99  // Interni chyba
#define INTERNAL_ERROR   -99  // Interni chyba
#define NOT_FOUND        -404 // Nebylo nalezeno v tabulce
#define SUCCESS           1

#include <stdbool.h>

#endif /*CONSTANTS_H_INCLUDED*/
