/* ******************************** parser.c ******************************** */
/* Soubor:              ..........................................            */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:    -> Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                      Pavel Juhaňák         xjuhan01                        */
/*                   -> Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#include <stdio.h>
#include "constants.h"
#include "parser.h"
#include "scanner.h"
#include "str.h"
#include "main.h"
#include "ial.h"
#include "expr.h"
#include "instructions.h"
#include "interpret.h"

#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_RESET   "\x1b[0m"
// in an out of love

// makro nám ošetřuje chyby, které detekuje parser (syntaktické, sémantické)
#define HANDLE_PARSER_ERRORS(errcode) { \
  if (errcode != SCAN_ERROR) { \
    switch(errcode){ \
    case EPARS: \
      fprintf(stderr, "SYNTAX ERROR detected in file: %s on line %d. Current token value = %s, token type = %d. Current input line = %d. Current input char = %d", __FILE__, __LINE__, tokenString.str, tokenType, errRow, errChar); \
      break; \
    case ESEM_D: \
    case ESEM_M: \
    case ESEM_E: \
      fprintf(stderr, "SEMANTIC ERROR detected in file: %s on line %d. Current token value = %s, token type = %d. Current input line = %d. Current input char = %d", __FILE__, __LINE__, tokenString.str, tokenType, errRow, errChar); \
      break; \
    } \
  } \
  return errcode; \
} \

tListOfInstr list;                           // globalni promenna uchovavajici seznam instrukci
tListItem* bootInstruction;                  // ukazatel na zaváděcí instrukci
tInstructStack ifInstructStack;              // zásobník instrukcí I_IF
tInstructStack elseInstructStack;            // zásobník instrukcí I_ELSE
tInstructStack exprWhileInstructStack;       // zásobník instrukcí, kterýmy začínají výrazy v cyklech while
tInstructStack whileInstructStack;           // zásobník instrukcí I_WHILE

bool isFirstInstructionInFunc = false;       // příznak, který určuje jestli se jedná o první instrukci funkce
bool vestFuncCall = false;                   // příznak, jestli právě voláme vestavěnou funkci
bool lastKeywordWasElse = false;             // příznak, který určuje jestli bylo načteno naposled klíčové slovo else ajsme tedy v bloku else
bool lastInstructWasAfterBlockElse = false;  // příznak, který určuje jestli byla poslední instrukce, instrukce za blokem else
bool lastInstructWasAfterBlockWhile = false; // příznak, který určuje jestli byla poslední instrukce, instrukce za blokem while
bool lastInstructWasBoot = false;            // příznak, který určuje jestli byla poslední instrukce, bootovací instrukcí
bool lastInstructWasWhileCondition = false;  // příznak, který určuje jestli byla poslední instrukce, první instrukcí v podmínce cyklu while

int counterVar = 1;                          // číslo vygenerované proměnné

Data *currentFunction = NULL;                // ukazatel na současnou funkci ve které se nacházíme

table globalTS;                              // instantce globální tabulky symbolů
table* localTS;                              // ukazatel na aktuální instanci lokální tabulky symbolů

int context = CONTEXT_GLOBAL;                // kontext ve kterém se při syntaktické a sémantické analýze nacházíme - může být globální nebo lokální

int tokenType;                               // typ načteného tokenu
string tokenString;                          // načtený token reprezentován jako jako řetezec

typedef struct tokenData{                    // struktura která uchovává data o tokenu
  int tokenType;                             // typ tokenu
  string* tokenString;                       // nazev tokenu
} tokenData;

// pole použité jako buffer, kde se budou ukládat načtené tokeny v případě kdy se budeme rozhodovat,
// jestli se jedná o přiřazení hodnoty výrazu, nebo výsledku volání funkce
// v takovém případě můžeme načíst až 2 tokeny, než zjistíme o kterých ze dvou případů se jedná
tokenData* bufferOfTokens[] = {NULL, NULL};

// prototypy použitých funkcí v rámci rekurzivního sestupu
int program();
int varsDefSect();
int varDef();
int varDefNext();
int varType(tableItem* declaredVarPtr);
int func();
int forwOrBody(bool funcForwDeclared, tableItem *funcPtr);
int params(bool funcForwDeclared, tableItem *funcPtr);
int paramsN(bool funcForwDeclared, tableItem *funcPtr, int paramPos);
int body();
int bodyNonEps();
int commAssign();
int commComp();
int commIf();
int commWhile();
int commRead();
int commWrite();
int rightAssign(tableItem* variable);
int terms(tableItem* funcPtr, int paramPos);
int termsN(tableItem* funcPtr, int paramPos);
int bodyContinue();
int expr(int breakpoint, int expectedDataType, Data *variableData);
bool invalidToken(int);

// prototypy funkcí pro práci s tokeny
int buffGetToken(string *tokenString);
void buffSaveToken(string *tokenString, int tokenType, int *Error);
void buffDeleteOnIndex(int index);
void buffTruncate();

// prototypy použitých funkcí v rámci sémantické analýzy
void fillGlobalTSbyBuildInFunctions(int *Error);
bool isVariableDeclared(string *tokenString);
bool functionExists(string *tokenString);
bool forEachForwDecExistFuncDef();
bool paramNamesAreEqual(tableItem *funcPtr, int paramPos, string *tokenString);
string* getParamOnPosition(tableItem *funcPtr, int paramPos);
bool variableExists(string *tokenString);
int getDataTypeIntFromChar(char dataType);
int getDataTypeIntFromLit(int litType);

// prototypy funckí využité při generování vnitřního kódu
void generateVariable(string *var);
void generateInstruction(int instType, void *addr1, void *addr2, void *addr3);

// úklidové funkce
void cleanResources();

int callParser(){ // funkce, která koordinuje činnost lexikální, syntaktické a sémantické analýzy a také interpretu
  int retCode;
  int Error;

  listInit(&list); // funkce inicializuje seznam instrukci

  // inicializace zásobníků instrukcí
  instructStackInit(&ifInstructStack);
  instructStackInit(&elseInstructStack);
  instructStackInit(&exprWhileInstructStack);
  instructStackInit(&whileInstructStack);

  // inicializujeme globální tabulku symbolů a globální ukazatel na aktuální lokální tabulku symbolů
  localTS = NULL;
  newTable(&globalTS);

  // naplníme tabulku symbolů vestavěnými funkcemi
  fillGlobalTSbyBuildInFunctions(&Error);
  if(Error != SUCCESS) return INTERNAL_ERROR;

  if (strInit(&tokenString) == STR_ERROR){ // inicializujeme proměnnou reprezentujcí token
    return INTERNAL_ERROR;
  }

  tokenType = getToken(&tokenString); // načteme první token

  retCode = program(); // započneme rekurzivní sestup

  if (retCode != EOK){ // pokud nastane chyb uklidíme zdroje a ukončíme interpret
    cleanResources();  // uvolníme použité zdroje

    if (retCode == INTERNAL_ERROR){ // pokud se jedná o interní chybu vrátíme příslušný chybový kód
      return EINT;
    }
    else if(retCode == SCAN_ERROR){ // pokud se jedná o chybu scanneru vrátíme příslušný chybový kód
      return ESCAN;
    }
    else return retCode; // pokud parser neproběhl v pořádku ukončíme jej s chybovou hláškou
  }

  //vypisInstrukce(); // debugovací funkce pro přehledný výpis vygenerovaných instrukcí

  retCode = interpret(&globalTS, &list, bootInstruction); // zavoláme interpret pro interpretování seznamu instrukcí

  cleanResources(); // uvolníme použité zdroje
  return retCode; // vrátíme chybový nebo bezchybový kód parseru
}

/* ************************************************************************************************************* */
/* *********************** ZAČÁTEK SYNTAKTICKÉ ANALÝZY POMOCÍ METODY REKURZIVNÍHO SESTUPU ********************** */
/* ************************************************************************************************************* */

/**
* Simulace neterminalu <PROGRAM>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int program(){
  int retCode;

  /// simulace pravidla 1: <PROGRAM> -> <VARS_DEF_SECT> <FUNC> begin <BODY> end . $  ///

  if(tokenType == KW_VAR){ /// <VARS_DEF_SECT> ///

    if((retCode = varsDefSect()) != EOK){
      return retCode;
    }
  }
  else if(invalidToken(tokenType)) return tokenType;

  if(tokenType == KW_FUNCTION){ /// <FUNC> ///

    if((retCode = func()) != EOK){
      return retCode;
    }
  }
  else if(invalidToken(tokenType)) return tokenType;

  if(tokenType == KW_BEGIN){ /// begin ///

    /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

    lastInstructWasBoot = true; // nastavíme příznak, že následující instrukce se má uložit jako zavádějící

    /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

    if((retCode = body()) != EOK){ /// <BODY> ///
      return retCode;
    }

  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);

  // z body() je uz nacteny a overeny token end, nacitame tedy nasledujici token
  if((tokenType = getToken(&tokenString)) == DOT){ /// . ///

    if((tokenType = getToken(&tokenString)) == END){ /// $ ///

      /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

      // pokud ke každé dopředné deklaraci neexistuje odpovídající definice hlásíme sémantickou chybu 3
      if(forEachForwDecExistFuncDef() == false) HANDLE_PARSER_ERRORS(ESEM_D);

      /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

      /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

      generateInstruction(I_HALT, (void*) NULL, (void*) NULL, (void*) NULL); // generujeme instrukci, která ukončí provádění instrukcí a tedy i vstupního programu

      /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

      return EOK;
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <VARS_DEF_SECT>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int varsDefSect(){
  /// simulace pravidla 2: <VARS_DEF_SECT> -> var <VAR_DEF> ///

  // token var mame nacteny a overeny z funkce program() nebo funkce forwOrBody()
  // pred volanim varDef musime nacist jeji prvni token
  tokenType = getToken(&tokenString);

  return varDef(); /// <VAR_DEF> ///
}

/**
* Simulace neterminalu <VAR_DEF>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int varDef(){
  int retCode;
  int Error;

  /// simulace pravidla 4: <VAR_DEF> -> id : <VAR_TYPE> ; <VAR_DEF_NEXT> ///

  // token id uz mame nacteny z funkce varDefNext nebo funkce varsDefSect, jen jej zkontrolujeme
  if(tokenType == ID){ /// id ///

    /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

    // pokud proměnná kterou se pokoušíme deklarovat, je už v daném kontextu deklarovaná, dojde k sémantické chybě
    if (isVariableDeclared(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);

    // pokud již existuje funkce se stejným jménem jako proměnná, dojde k sémantické chybě
    if (functionExists(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);

    // uložíme nově deklarovanou proměnnou do příslušné TS podle místa kde proměnné deklarujeme
    table *TS = (context == CONTEXT_GLOBAL) ? &globalTS : localTS;
    tableItem* declaredVarPtr = saveItem(TS, &tokenString, VARIABLE, &Error);
    if (Error != SUCCESS) return Error;

    /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

    if((tokenType = getToken(&tokenString)) == COLON){ /// : ///

      if((retCode = varType(declaredVarPtr)) == EOK){ /// <VAR_TYPE> ///

        if((tokenType = getToken(&tokenString)) == SEMICOLON){ /// ; ///

          if((retCode = varDefNext()) == EOK){ /// <VAR_DEF_NEXT> ///
            return EOK;
          }
          else return retCode;
        }
        else if(invalidToken(tokenType)) return tokenType;
        else HANDLE_PARSER_ERRORS(EPARS);
      }
      else return retCode;
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <VAR_DEF_NEXT>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int varDefNext(){
    /// simulace pravidla 5: <VAR_DEF_NEXT> -> <VAR_DEF> ///
    if((tokenType = getToken(&tokenString)) == ID){ /// <VAR_DEF> ///
        return varDef();
    }
    /// simulace pravidla 47: <VAR_DEF_NEXT> -> eps ///
    else if(tokenType == KW_FUNCTION || tokenType == KW_BEGIN){ /// eps ///
      return EOK; // je jiz nacten dalsi token
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <VAR_TYPE>
* @param declaredVarPtr ukazatel do TS na nově deklarovanou proměnnou nebo NULL, pokud se jedná o parametr v hlaviččce funkce
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int varType(tableItem* declaredVarPtr){
  tokenType = getToken(&tokenString);

  /// simulace pravidel 6,7,8,9: <VAR_TYPE> -> integer || <VAR_TYPE> -> real || <VAR_TYPE> -> string || <VAR_TYPE> -> boolean ///
  if(tokenType == DT_BOOLEAN || tokenType == DT_INTEGER || tokenType == DT_REAL || tokenType == DT_STRING){

    /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

    // dodáme do TS k nově deklarované proměnné její datový typ
    if (declaredVarPtr != NULL) declaredVarPtr->data->dataType = tokenType;

    // inicializujeme proměnnou typu string
    if(declaredVarPtr != NULL && declaredVarPtr->data->dataType == DT_STRING
       && strInit(&(declaredVarPtr->data->value.strVal)) == STR_ERROR) return INTERNAL_ERROR;

    /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

    return EOK;
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <FUNC>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int func(){
  int retCode;
  int Error;
  bool funcForwDeclared = false;   // příznak jestli je již funkce dopředně deklarována
  tableItem* funcPtr = NULL;       // ukazatel do TS na nově (v případě stejného identifikátoru na již dříve) deklarovanou funkci
  table *tmpLocalTS = NULL;        // ukazatel na mallocovanou lokální tabulku symbolů pro tuto funci
  isFirstInstructionInFunc = true; // příznak, který určuje jestli se jedná o první instrukci funkce nastavíme na true

  /// simulace pravidla 10: <FUNC> -> function id (<PARAMS>) : <VAR_TYPE> ; <FORW_OR_BODY> <FUNC> ///

  // token function uz mame nacteny, ale presto jej radsi overime
  if(tokenType == KW_FUNCTION){ /// function ///

    if((tokenType = getToken(&tokenString)) == ID){ /// id ///

      /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

      // pokud je již na globální úrovni definována proměnná se stejným jménem jako funkce, dojde k sémantické chybě
      if (isVariableDeclared(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);

      if (functionExists(&tokenString)){ // pokud funkce se stejným jménem již v TS existuje, nalezneme ji

        funcPtr = searchItem(&globalTS, &tokenString, FUNCTION, &Error);

        // pokud je již funkce se stejným  názvem definována, tak dojde k chybě
        if(funcPtr->data->defined == true) HANDLE_PARSER_ERRORS(ESEM_D);

        // pokud funkce existuje v TS a není definována, tak musí být zákonitě dopředně deklarována
        funcForwDeclared = true;

        free(funcPtr->data->localTS); // pokud už funkce se stejným jménem existuje, tak smažeme její lokální tabulku symbolů
        funcPtr->data->localTS = NULL;
      }
      else{ // pokud funkce se stejným jménem doposud v TS neexituje
        // uložíme nově deklarovanou nebo definovanou funkci do globální TS
        funcPtr = saveItem(&globalTS, &tokenString, FUNCTION, &Error);
        if (Error != SUCCESS) return Error;
      }

      // vytvoříme novou tabulku symobolů
      tmpLocalTS = malloc(sizeof(table));
      newTable(tmpLocalTS);
      localTS = tmpLocalTS;                // novou TS přiřadíme současné funkci
      funcPtr->data->localTS = tmpLocalTS; // a přiřadíme ji globálnímu ukazateli na aktuální lokální TS

      context = CONTEXT_LOCAL;             // nastavíme kontext na lokální
      currentFunction = funcPtr->data;     // uložíme si odkaz na současnou funkci

      /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

      if((tokenType = getToken(&tokenString)) == BRACKET_L){ /// ( ///

        if((retCode = params(funcForwDeclared, funcPtr)) != EOK){ /// <PARAMS> ///
          return retCode;
        }

        // token ) uz je nacteny, jen jej pro jistotu zkontrolujeme
        if(tokenType == BRACKET_R){ /// ) ///

          if((tokenType = getToken(&tokenString)) == COLON){ /// : ///

            if((retCode = varType(NULL)) != EOK){ /// <VAR_TYPE> ///
              return retCode;
            }

            /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

            if(funcForwDeclared){ // pokud byla funkce dopředně deklarována
              // pokud nesedí datový typ dříve deklarované a nyní definované funkce vrátíme sémantickou chybu 3
              if(funcPtr->data->dataType != tokenType) HANDLE_PARSER_ERRORS(ESEM_D);
            }
            else{ // uložíme si datový typ návratové hodnoty funkce
              funcPtr->data->dataType = tokenType;
            }

            // do lokálni tabulky symbolů uložíme novou lokální proměnnou, která představuje výstupní hodnotu funkce
            tableItem *item = saveItem(localTS, funcPtr->data->name, VARIABLE, &Error);
            if (Error != SUCCESS) return Error;

            // proměnné ve které bude návratová hodnota funkce nastavíme datový typ shodný s návratovým typem funkce
            item->data->dataType = funcPtr->data->dataType;
            if(strCpy(item->data->name, funcPtr->data->name) != STR_SUCCESS){
              return INTERNAL_ERROR;
            }

            /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

            if((tokenType = getToken(&tokenString)) == SEMICOLON){ /// ; ///

              if((retCode = forwOrBody(funcForwDeclared, funcPtr)) != EOK){ /// <FORW_OR_BODY> ///
                return retCode;
              }

              if((tokenType = getToken(&tokenString)) == KW_FUNCTION){ /// <FUNC> -> function ///
                return func();
              }

              else if(tokenType == KW_BEGIN){ /// <FUNC> -> eps ///
                return EOK;
              }
              else if(invalidToken(tokenType)) return tokenType;
              else HANDLE_PARSER_ERRORS(EPARS);
            }
            else if(invalidToken(tokenType)) return tokenType;
            else HANDLE_PARSER_ERRORS(EPARS);
          }
          else if(invalidToken(tokenType)) return tokenType;
          else HANDLE_PARSER_ERRORS(EPARS);
        }
        else if(invalidToken(tokenType)) return tokenType;
        else HANDLE_PARSER_ERRORS(EPARS);
      }
      else if(invalidToken(tokenType)) return tokenType;
      else HANDLE_PARSER_ERRORS(EPARS);
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);

}

/**
* Simulace neterminalu <FORW_OR_BODY>
* @param funcForwDeclared Příznak, jestli již byla funkce dříve dopředně deklarována
* @param funcPtr ukazazatel na funkci v TS - buď již dříve deklarovanou/definovanou nebo nyní čerstvě deklarovanou/definovanou
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int forwOrBody(bool funcForwDeclared, tableItem *funcPtr){
  int retCode;
  int Error;

  /// simulace pravidla 12: <FORW_OR_BODY> -> forward ; ///
  if((tokenType = getToken(&tokenString)) == KW_FORWARD){ /// forward ///

    if((tokenType = getToken(&tokenString)) == SEMICOLON){ /// ; ///

      /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

      // pokud již funkce je dopředně deklarována, není možné ji znovu předeklarovat
      if (funcForwDeclared) HANDLE_PARSER_ERRORS(ESEM_D);

      funcPtr->data->declared = true;   // nastavíme příznak deklarace funkce na true
      context = CONTEXT_GLOBAL;         // nastavíme kontext na globální
      currentFunction = NULL;           // uložíme si odkaz na současnou funkci
      isFirstInstructionInFunc = false; // deaktivujeme příznak první instrukce ve funkci
      return EOK;

      /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  else if(invalidToken(tokenType)) return tokenType;

  /// simulace pravidla 13: <FORW_OR_BODY> -> <VARS_DEF_SECT> begin <BODY> end ; ///
  else{

    if(tokenType == KW_VAR){ /// <VARS_DEF_SECT> ///
      if((retCode = varsDefSect()) != EOK){
        return retCode;
      }
    }
    if(tokenType == KW_BEGIN){ /// begin ///

    /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

    // do lokální TS uložíme proměnnou $returnValue, ve které bude návratová hodnota funkce
    string returnValue;
    returnValue.str = "$returnValue";
    returnValue.length = strlen(returnValue.str);
    returnValue.allocSize = strlen(returnValue.str);
    tableItem *returnValueItem = saveItem(localTS, &returnValue, VARIABLE, &Error);

    if(Error != SUCCESS) return INTERNAL_ERROR;
    returnValueItem->data->dataType = funcPtr->data->dataType;

    // do lokální TS uložíme proměnnou $returnAddress, ve které bude návratová adresa funkce
    string returnAddress;
    returnAddress.str = "$returnAddress";
    returnAddress.length = strlen(returnAddress.str);
    returnAddress.allocSize = strlen(returnAddress.str);
    tableItem *returnAddressItem = saveItem(localTS, &returnAddress, VARIABLE, &Error);
    if(Error != SUCCESS) return INTERNAL_ERROR;

    /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

      if((retCode = body()) != EOK) { /// <BODY> ///
        return retCode;
      }
      // token end uz mame nacteny z body() jen jej zkontrolujeme
      if(tokenType == KW_END){ /// end ///

        if((tokenType = getToken(&tokenString)) == SEMICOLON){ /// ; ///

          /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

          funcPtr->data->defined = true;  // nastavíme příznak definice funkce na true
          funcPtr->data->declared = true; // nastavíme příznak deklarace funkce na true

          context = CONTEXT_GLOBAL;       // nastavíme kontext zpět na globální

          /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

          // nalezneme v lokální TS pro danou funkci proměnnou, která má stejný název jako funkce
          tableItem* varSameNameAsFunc = searchItem(funcPtr->data->localTS, funcPtr->data->name, VARIABLE, &Error);

          // generujeme instrukci, která přiřadí do proměnn $returnValue proměnnou, která má stejné jméno jako funkce
          generateInstruction(I_ASSIGN, (void*) (varSameNameAsFunc->data), (void*) NULL, (void*) (returnValueItem->data));

          // generujeme instrukci, která dá pokyn překladači k návratu z funkce
          generateInstruction(I_RETURN, (void*) NULL, (void*) NULL, (void*) NULL);

          /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

          isFirstInstructionInFunc = false; // deaktivujeme příznak první instrukce ve funkci
          currentFunction = NULL;           // uložíme si odkaz na současnou funkci
          return EOK;

          /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

        }
        else if(invalidToken(tokenType)) return tokenType;
        else HANDLE_PARSER_ERRORS(EPARS);
      }
      else if(invalidToken(tokenType)) return tokenType;
      else HANDLE_PARSER_ERRORS(EPARS);
    }
    else HANDLE_PARSER_ERRORS(EPARS);
  }
}

/**
* Simulace neterminalu <PARAMS>
* @param funcForwDeclared Příznak, jestli již byla funkce dříve dopředně deklarována
* @param funcPtr ukazazatel na funkci v TS - buď již dříve deklarovanou/definovanou nebo nyní čerstvě deklarovanou/definovanou
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int params(bool funcForwDeclared, tableItem *funcPtr){
  int retCode;
  int Error;
  int paramPos = 0;
  tableItem* declaredParamPtr;

  /// simulace pravidla 14: <PARAMS> -> id : <VAR_TYPE> <PARAMS_N> ///
  if((tokenType = getToken(&tokenString)) == ID){ /// id ///

    /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

    // pokud již existuje funkce se stejným jménem jako název parametru, nebo je již parametr se stejným jménem deklarován, dojde k sémantické chybě 3
    if (functionExists(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);
    if (isVariableDeclared(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);

    if(funcForwDeclared){ // pokud už funkce byla dopředně deklarována, tak budeme kontrolovat shodnost parametrů
      // pokud se parametr neshoduje s předpokládaným parametrem - např. k danému parametru neexistuje ve dopředné deklaraci žádný další parametr,
      // nebo je typ parametru nestejný s parametrem uvedeným v dopředné deklaraci, tak budeme hlásit sémantickou chybu 3
      if(strLen(funcPtr->data->paramsTypes) < (paramPos + 1) || !paramNamesAreEqual(funcPtr, paramPos, &tokenString)){
        HANDLE_PARSER_ERRORS(ESEM_D);
      }
    }
    else{ // pokud zatím funkce nebyla dopředně deklarována, tak budeme názvy parametrů ukládat k dané funkci do TS
      addNextFuncParam(funcPtr, paramPos, &tokenString, &Error);
      if(Error != SUCCESS) return INTERNAL_ERROR;
    }

    // uložíme parametr jako lokální proměnnou do lokální TS
    declaredParamPtr = saveItem(localTS, &tokenString, VARIABLE, &Error);
    if (Error != SUCCESS) return Error;

    /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

    if((tokenType = getToken(&tokenString)) == COLON){ /// : ///

      if((retCode = varType(declaredParamPtr)) != EOK){ /// <VAR_TYPE> ///
        return retCode;
      }

      /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

      // určíme typ parametru, který je v posledním načteném tokenu
      char paramType;
      switch(tokenType){
      case DT_INTEGER:
        paramType = 'i';
        break;
      case DT_REAL:
        paramType = 'r';
        break;
      case DT_STRING:
        paramType = 's';
        break;
      case DT_BOOLEAN:
        paramType = 'b';
        break;
      }

      if(funcForwDeclared){ // pokud již funkce se stejným jménem byla dopředně deklarována
        // pokud typ parametru není stejný jako typ odpovídajícího parametru v dopředně deklarované funkci, nastane chyba 3
        if (paramType != funcPtr->data->paramsTypes->str[paramPos]){
          HANDLE_PARSER_ERRORS(ESEM_D);
        }
      }
      else{
        // přidáme další parametr pro danou funkci
        if (strCat(funcPtr->data->paramsTypes, paramType) != STR_SUCCESS) return INTERNAL_ERROR;
      }

      // k uloženému parametru jako lokální proměnné v TS dodáme informaci o jeho typu a nastavíme příznak deklarace
      declaredParamPtr->data->dataType = tokenType;
      declaredParamPtr->data->declared = true;

      /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

      return paramsN(funcForwDeclared, funcPtr, ++paramPos); /// <PARAMS_N> ///
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  /// simulace pravidla 15: <PARAMS> -> eps ///
  else if(tokenType == BRACKET_R){

    /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

    if(funcForwDeclared){ // pokud již funkce se stejným jménem byla dopředně deklarována
      // pokud nesouhlasí počet parametrů dopředně deklarované a nyní definované funkce dojde k sémantické chybě 3
      if(strLen(funcPtr->data->paramsTypes) != paramPos) HANDLE_PARSER_ERRORS(ESEM_D);
    }

    /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

    return EOK;
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <PARAMS_N>
* @param funcForwDeclared Příznak, jestli již byla funkce dříve deklarována/definována
* @param funcPtr ukazazatel na funkci v TS - buď již dříve deklarovanou/definovanou nebo nyní čerstvě deklarovanou/definovanou
* @param paramPos Pozice parametru v hlavičce funkce
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int paramsN(bool funcForwDeclared, tableItem *funcPtr, int paramPos){
  int retCode;
  int Error;
  tableItem* declaredParamPtr;

  /// simulace pravidla 16: <PARAMS_N> -> ; id : <VAR_TYPE> <PARAMS_N> ///
  if((tokenType = getToken(&tokenString)) == SEMICOLON){ /// ; ///

    if((tokenType = getToken(&tokenString)) == ID){ /// id ///

      /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

      // pokud již existuje funkce se stejným jménem jako název parametru, nebo je již parametr se stejným jménem deklarován, dojde k sémantické chybě
      if (functionExists(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);
      if (isVariableDeclared(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);

      if(funcForwDeclared){ // pokud už funkce byla dopředně deklarována, tak budeme konstrolovat shodnost parametrů
        // pokud se parametr neshoduje s předpokládaným parametrem - např. k danému parametru neexistuje ve dopředné deklaraci žádný další parametr,
        // nebo je typ parametru nestejný s parametrem uvedeným v dopředné deklaraci, tak budeme hlásit sémantickou chybu 3
        if(strLen(funcPtr->data->paramsTypes) < (paramPos + 1) || !paramNamesAreEqual(funcPtr, paramPos, &tokenString)){
          HANDLE_PARSER_ERRORS(ESEM_D);
        }
      }
      else{ // pokud zatím funkce nebyla dopředně deklarována, tak budeme názvy parametrů ukládat k dané funkci do TS
        addNextFuncParam(funcPtr, paramPos, &tokenString, &Error);
        if(Error != SUCCESS) return INTERNAL_ERROR;
      }

      // uložíme parametr jako lokální proměnnou do lokální TS
      declaredParamPtr = saveItem(localTS, &tokenString, VARIABLE, &Error);
      if (Error != SUCCESS) return Error;

      /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

      if((tokenType = getToken(&tokenString)) == COLON){ /// : ///

        if((retCode = varType(declaredParamPtr)) != EOK){ /// <VAR_TYPE> ///
          return retCode;
        }

        /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

        // určíme typ parametru, který je v posledním načteném tokenu
        char paramType;
        switch(tokenType){
        case DT_INTEGER:
          paramType = 'i';
          break;
        case DT_REAL:
          paramType = 'r';
          break;
        case DT_STRING:
          paramType = 's';
          break;
        case DT_BOOLEAN:
          paramType = 'b';
          break;
        }

        if(funcForwDeclared){ // pokud již funkce se stejným jménem byla dopředně deklarována
          // pokud typ parametru není stejný jako typ odpovídajícího parametru v dopředně deklarované funkci, nastane chyba 3
          if (paramType != funcPtr->data->paramsTypes->str[paramPos]){
            HANDLE_PARSER_ERRORS(ESEM_D);
          }
        }
        else{
          // přidáme další parametr pro danou funkci
          if (strCat(funcPtr->data->paramsTypes, paramType) != STR_SUCCESS) return INTERNAL_ERROR;
        }

        // k uloženému parametru jako lokální proměnné v TS dodáme informaci o jeho typu a nastavíme příznak deklarace
        declaredParamPtr->data->dataType = tokenType;
        declaredParamPtr->data->declared = true;

        /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

        return paramsN(funcForwDeclared, funcPtr, ++paramPos); /// <PARAMS_N> ///
      }
      else if(invalidToken(tokenType)) return tokenType;
      else HANDLE_PARSER_ERRORS(EPARS);
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  /// simulace pravidla 17: <PARAMS_N> -> eps ///
  else if(tokenType == BRACKET_R){

    /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

    if(funcForwDeclared){ // pokud již funkce se stejným jménem byla dopředně deklarována
      // pokud nesouhlasí počet parametrů dopředně deklarované a nyní definované funkce dojde k sémantické chybě 3
      if(strLen(funcPtr->data->paramsTypes) != paramPos) HANDLE_PARSER_ERRORS(ESEM_D);
    }

    /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

    return EOK;
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <BODY>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int body(){
  int retCode;
  /// simulace pravidla 18: <BODY> -> <COMM_ASSIGN> ///
  if ((tokenType = getToken(&tokenString)) == ID){
    return commAssign();
  }
  /// simulace pravidla 19: <BODY> -> <COMM_COMP> ///
  else if(tokenType == KW_BEGIN){
    return commComp();
  }
  /// simulace pravidla 20: <BODY> -> <COMM_IF> ///
  else if(tokenType == KW_IF){
    return commIf();
  }
  /// simulace pravidla 21: <BODY> -> <COMM_WHILE> ///
  else if(tokenType == KW_WHILE){
    return commWhile();
  }
  /// simulace pravidla 22: <BODY> -> <COMM_READ> ///
  else if(tokenType == KW_READLN){
    return commRead();
  }
  /// simulace pravidla 23: <BODY> -> <COMM_WRITE> ///
  else if(tokenType == KW_WRITE){
    return commWrite();
  }
  /// simulace pravidla 24: <BODY> -> eps ///
  else if(tokenType == KW_END){
    return EOK;
  }
  else if(invalidToken(tokenType)) return tokenType;
  // jinak chyba syntaxe
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <BODY_NON_EPS>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int bodyNonEps(){
  /// simulace pravidla 25: <BODY_NON_EPS> -> <COMM_ASSIGN> ///
  if ((tokenType = getToken(&tokenString)) == ID){
    return commAssign();
  }
  /// simulace pravidla 26: <BODY_NON_EPS> -> <COMM_COMP> ///
  else if(tokenType == KW_BEGIN){
    return commComp();
  }
  /// simulace pravidla 27: <BODY_NON_EPS> -> <COMM_IF> ///
  else if(tokenType == KW_IF){
    return commIf();
  }
  /// simulace pravidla 28: <BODY_NON_EPS> -> <COMM_WHILE> ///
  else if(tokenType == KW_WHILE){
    return commWhile();
  }
  /// simulace pravidla 29: <BODY_NON_EPS> -> <COMM_READ> ///
  else if(tokenType == KW_READLN){
    return commRead();
  }
  /// simulace pravidla 30: <BODY_NON_EPS> -> <COMM_WRITE> ///
  else if(tokenType == KW_WRITE){
    return commWrite();
  }
  else if(invalidToken(tokenType)) return tokenType;
  // jinak chyba syntaxe
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <COMM_ASSIGN>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int commAssign(){
  int retCode;
  int Error;
  tableItem* variable;
  Data *resultPtr; // výsledek výrazu, nebo volání funkce
  // token id uz mame nacteny a overeny z <BODY> nebo <BODY_NON_EPS>

  /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

  // ověříme, jestli je proměnná do které přiřazujeme definovaná v daném kontextu,
  // pokud proměnná není definována vrátíme sémantickou chybu 3
  if(!variableExists(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);

  // nalezneme v TS proměnnou do které budeme přiřazovat
  table *TS = (getContextOfVariable(&tokenString) == CONTEXT_GLOBAL) ? &globalTS : localTS;
  variable = searchItem(TS, &tokenString, VARIABLE, &Error); // k dané proměnné si v TS nalezneme informace

  /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

  /// simulace pravidla 31: <COMM_ASSIGN> -> id := <RIGHT_ASSIGN> <BODY_CONTINUE> ///
  if((tokenType = getToken(&tokenString)) == OP_ASSIGN){ /// := ///

    if((retCode = rightAssign(variable)) == EOK){ /// <RIGHT_ASSIGN> ///

      // <RIGHT_ASSIGN> nam nacetl dalsi token, tedy token z <BODY_CONTINUE>
      return bodyContinue(); /// <BODY_CONTINUE> ///
    }
    else return retCode;
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <COMM_COMP>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int commComp(){
  int retCode;

  // token begin uz mame nacteny a overeny z <BODY> nebo <BOY_NON_EPS>
  /// simulace pravidla 32: <COMM_COMP> -> begin <BODY> end <BODY_CONTINUE> ///

  if ((retCode = body()) == EOK){ /// <BODY> ///
    // token end uz mame nacteny z <BODY>, jen jej overime
    if (tokenType == KW_END){ /// end ///
      // nacteme dalsi token, zde porusujeme zasadu nenacitat token, ktery je soucasti nasledujiciho neterminalu
      // ale pred volanim funkce bodyContinue() tak musime ucinit
      tokenType = getToken(&tokenString);

      return bodyContinue(); /// <BODY_CONTINUE> ///
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  else return retCode;
}

/**
* Simulace neterminalu <COMM_IF>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int commIf(){
  int retCode;
  Data *exprResultData = NULL; // ukazatel na výsledek výrazu

  // token if uz mame nacteny a overeny z <BODY> nebo <BOY_NON_EPS>
  /// simulace pravidla 33: <COMM_IF> -> if <EXPR> then begin <BODY> end else begin <BODY> end <BODY_CONTINUE> ///
  if((retCode = expr(KW_THEN, DT_BOOLEAN, exprResultData)) == EOK){ /// <EXPR> ///

    /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

    // vložíme si na zásobník lastIfInstruc ukazatel na vygenerovanou instrukci I_IF
    if (instructPush(&ifInstructStack, list.last) != INST_SUCCESS) return INTERNAL_ERROR;

    /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

    // token then uz mame nacteny z <EXPR>, jen jej pro jistotu overime
    if(tokenType == KW_THEN){ /// then ///

      if((tokenType = getToken(&tokenString)) == KW_BEGIN){ /// begin ///

        if((retCode = body()) == EOK){ /// <BODY> ///

          /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

          // generujeme instrukci, která dá pokyn překladači k přeskočen bloku else
          generateInstruction(I_ELSE, (void*) NULL, (void*) NULL, (void*) NULL);

          // vložíme si na zásobník elseInstructStack ukazatel na vygenerovanou instrukci I_ELSE
          if (instructPush(&elseInstructStack, list.last) != INST_SUCCESS) return INTERNAL_ERROR;

          /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

          // token end mame uz nacteny z <BODY>, nacitame else
          if((tokenType = getToken(&tokenString)) == KW_ELSE){ /// else ///

            if((tokenType = getToken(&tokenString)) == KW_BEGIN){ /// begin ///

              /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

              lastKeywordWasElse = true; // poslední načtený token bylo klíčové slovo else

              /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

              if((retCode = body()) == EOK){ /// <BODY> ///
                // token end uz mem nacteny z <BODY> jen jej overime
                if(tokenType == KW_END){ /// end ///

                  /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

                  lastInstructWasAfterBlockElse = true; // příznak, který určuje jestli byla poslední instrukce, instrukce za blokem else

                  /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

                  // nacteme dalsi token, zde porusujeme zasadu nenacitat token, ktery je soucasti nasledujiciho neterminalu
                  tokenType = getToken(&tokenString);

                  return bodyContinue(); /// <BODY_CONTINUE> ///
                }
                else if(invalidToken(tokenType)) return tokenType;
                else HANDLE_PARSER_ERRORS(EPARS);
              }
              else return retCode;
            }
            else if(invalidToken(tokenType)) return tokenType;
            else HANDLE_PARSER_ERRORS(EPARS);
          }
          else if(invalidToken(tokenType)) return tokenType;
          else HANDLE_PARSER_ERRORS(EPARS);
        }
        else return retCode;
      }
      else if(invalidToken(tokenType)) return tokenType;
      else HANDLE_PARSER_ERRORS(EPARS);
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  else return retCode;
}

/**
* Simulace neterminalu <COMM_WHILE>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int commWhile(){
  int retCode;
  Data *exprResultData = NULL; // ukazatel na výsledek výrazu

  // token while uz mame nacteny a overeny z <BODY> nebo <BOY_NON_EPS>

  lastInstructWasWhileCondition = true;
  /// simulace pravidla 34: <COMM_WHILE> -> while <EXPR> do begin <BODY> end <BODY_CONTINUE> ///
  if((retCode = expr(KW_DO, DT_BOOLEAN, exprResultData)) == EOK){ /// <EXPR> ///

    /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

    // vložíme si na zásobník whileInstructStack ukazatel na vygenerovanou instrukci I_WHILE
    if (instructPush(&whileInstructStack, list.last) != INST_SUCCESS) return INTERNAL_ERROR;

    /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

    // token do uz mame nacteny z <EXPR>, jen jej overime
    if(tokenType == KW_DO){ /// do ///

      if((tokenType = getToken(&tokenString)) == KW_BEGIN){ /// begin ///

        if((retCode = body()) == EOK){ /// <BODY> ///

          /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

          tListItem *lastWhileInstruc;                               // ukazatel na poslední odpovídající instrukci I_WHILE
          if(instructEmpty(&exprWhileInstructStack) == false){       // pokud na zásobníku instrukcí, kterými začínají výrazy ve while cyklech něco je, popneme to
            lastWhileInstruc = instructPop(&exprWhileInstructStack); // popneme si ukazatel na instrukci, kterpou začíná výraz vw while cyklu
          }

          // generujeme instrukci, která dá pokyn překladači k návratu na poslední instrukci WHILE, aby se znovu otestvala podmínka
          generateInstruction(I_REPEAT_WHILE, (void*) NULL, (void*) NULL, (void*) lastWhileInstruc);

          /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

          // token end mam nacteny z <BODY>, jen jej zkontrolujeme
          if (tokenType == KW_END){ /// end ///

            /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

            lastInstructWasAfterBlockWhile = true; // příznak, který určije, že následující instrukce bude první instrukcí za blokem while

            /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

            // nacitam token dopredu a tim porusuji dobre mravy, ale co se da delat, musim tak ucinit vzdy nez volam...
            tokenType = getToken(&tokenString);

            return bodyContinue(); /// <BODY_CONTINUE> ///
          }
          else if(invalidToken(tokenType)) return tokenType;
          else HANDLE_PARSER_ERRORS(EPARS);
        }
        else return retCode;
      }
      else if(invalidToken(tokenType)) return tokenType;
      else HANDLE_PARSER_ERRORS(EPARS);
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  else return retCode;
}

/**
* Simulace neterminalu <COMM_READ>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int commRead(){
  int Error;
  // token readln uz mame nacteny z body() nebo bodyNonEps(), jen jej overime

  /// simulace pravidla 35:<COMM_READ> -> readln (id) <BODY_CONTINUE> ///
  if(tokenType == KW_READLN){ /// readln ///

    if((tokenType = getToken(&tokenString)) == BRACKET_L){ /// ( ///

      if((tokenType = getToken(&tokenString)) == ID){ /// id ///

        /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

        // ověříme, jestli je proměnná do které načítáme vstup definovaná v daném kontextu,
        // pokud proměnná není definována vrátíme sémantickou chybu 3
        if(!variableExists(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);

        // nalezneme v TS odkaz na proměnnou
        table *TS = (getContextOfVariable(&tokenString) == CONTEXT_GLOBAL) ? &globalTS : localTS;
        tableItem* variable = searchItem(TS, &tokenString, VARIABLE, &Error);

        // pokud je proměnná typu boolean, dojde k sémantické chybě 4
        if(variable->data->dataType == DT_BOOLEAN) HANDLE_PARSER_ERRORS(ESEM_M);

        /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

        /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

        // generujeme instrukci, která dá pokyn překladači k načtení vstupu do proměnné
        generateInstruction(I_READLN, (void*) NULL, (void*) NULL, (void*) variable->data);

        /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

        if((tokenType = getToken(&tokenString)) == BRACKET_R){ /// ) ///
          // pred volanim funkce bodyContinue musime vzdy nacist jeji prvni token
          tokenType = getToken(&tokenString);

          return bodyContinue(); /// <BODY_CONTINUE> ///
        }
        else if(invalidToken(tokenType)) return tokenType;
        else HANDLE_PARSER_ERRORS(EPARS);
      }
      else if(invalidToken(tokenType)) return tokenType;
      else HANDLE_PARSER_ERRORS(EPARS);
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <COMM_WRITE>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int commWrite(){
  int retCode;

  /// simulace pravidla 36:<COMM_WRITE> -> write (<TERMS>) <BODY_CONTINUE> ///

  // token write uz mame nacteny z body() nebo bodyNonEps(), jen jej overime
  if(tokenType == KW_WRITE){ /// write ///

    if((tokenType = getToken(&tokenString)) == BRACKET_L){ /// ( ///

      if((retCode = terms(NULL, 0)) != EOK){ /// <TERMS> ///
        return retCode;
      }
      // token ) mame nacteny z funkce terms() jen jej overime
      if(tokenType == BRACKET_R){ /// ) ///
        //nacteni tokenu kvuli jeho nacitani v <EXPR>
        // neboli pred volanim funkce bodyContinue musime vzdy nacist jeji prvni token
        tokenType = getToken(&tokenString);

        return bodyContinue(); /// <BODY_CONTINUE> ///
      }
      else if(invalidToken(tokenType)) return tokenType;
      else HANDLE_PARSER_ERRORS(EPARS);
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <RIGHT_ASSIGN>
* @param variable proměnná do které přiřazujeme
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int rightAssign(tableItem* variable){
  int retCode;
  int Error;
  struct listItem* firstInstructInFunc;

  /// simulace pravidla 38: <RIGHT_ASSIGN> -> id ( <TERMS> ) ///
  /// simulace pravidla 37 : <RIGHT_ASSIGN> -> <EXPR>        ///
  if((tokenType = getToken(&tokenString)) == ID || tokenType == KW_SORT || tokenType == KW_FIND){ /// id ///

    // tento token si musime ulozit, protoze se muze jednat o cast vyrazu a my bychom nacetli dalsi token a tento bychom ztratili
    buffSaveToken(&tokenString, tokenType, &Error);
    if(Error != SUCCESS) return INTERNAL_ERROR;

    /// simulace pravidla 38: <RIGHT_ASSIGN> -> id ( <TERMS> ) ///
    if((tokenType = getToken(&tokenString)) == BRACKET_L){ /// ( ///

      /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

      // ověříme, jestli je funkce, kterou voláme definována,
      // pokud funkce není definována vrátíme sémantickou chybu 3
      if(!functionExists(bufferOfTokens[0]->tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);

      // nalezeneme podle identifikátoru funkce v TS
      tableItem *func = searchItem(&globalTS, bufferOfTokens[0]->tokenString, FUNCTION, &Error);

      // pokud se nerovnají typ proměnné kam přiřazujeme a typ výsledku, který vrací funkce, hlásíme sémantickou chybu 5
      if(func->data->dataType != variable->data->dataType) HANDLE_PARSER_ERRORS(ESEM_M);

      /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

      /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

      if (strcmp(func->data->name->str, "LENGTH") == 0
          || strcmp(func->data->name->str, "COPY") == 0
          || strcmp(func->data->name->str, "FIND") == 0
          || strcmp(func->data->name->str, "SORT") == 0){ // pokud voláme vestavěnoou funkci

        vestFuncCall = true;  // nastavíme příznak volání vestavěné funkce na true
      }
      else{
        vestFuncCall = false; // nastavíme příznak volání vestavěné funkce na false

        // generujeme instrukci, která vytvoří rámec pro volanou funkci
        generateInstruction(I_CREATE_FRAME, (void*) func->data, (void*) NULL, (void*) NULL);
      }

      /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

      if((retCode = terms(func, 0)) == EOK){ /// <TERMS> ///

        // token ) mame nacteny z funkce terms() jen jej overime
        if(tokenType == BRACKET_R){ /// ) ///
          // nasleduje body continue a pred jeho volanim je treba nacist prvni jeho token
          tokenType = getToken(&tokenString);
          buffTruncate(); // vyčistíme buffer, jinak by v něm zbyli data se kterými bychom později pak nechtěně pracovali

          /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

          if(vestFuncCall == true){ // pokud voláme vestavěnou funkci
            // generujeme instrukci, která zavolá vestavěnou funkci
            generateInstruction(I_CALL_VEST, (void*) func->data, (void*) NULL, (void*) variable->data);
          }
          else{ // pokud voláme nevestavěnou funkci
            // generujeme instrukci, která zavolá funkci
            generateInstruction(I_CALL, (void*) func->data, (void*) NULL, (void*) NULL);

            // generujeme instrukci, která přiřadí výsledek funkce do výsledné proměnné
            generateInstruction(I_SET_RESULT_AND_DESTROY_FRAME, (void*) NULL, (void*) NULL, (void*) variable->data);
          }

          /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

          return EOK;
        }
        else if(invalidToken(tokenType)) return tokenType;
        else HANDLE_PARSER_ERRORS(EPARS);
      }
      else return retCode;
    }
    else if(invalidToken(tokenType)) return tokenType;

    /// simulace pravidla 37 : <RIGHT_ASSIGN> -> <EXPR> ///
    else{ /// <EXPR> ///
      // tento token si musime ulozit nez zavolame EXPR
      buffSaveToken(&tokenString, tokenType, &Error);
      if(Error != SUCCESS) return INTERNAL_ERROR;

      return expr(SEMICOLON, variable->data->dataType, variable->data);
    }
  }
  else if(invalidToken(tokenType)) return tokenType;

  /// simulace pravidla 37 : <RIGHT_ASSIGN> -> <EXPR> ///
  else{ /// <EXPR> ///
    // tento token si musime ulozit nez zavolame EXPR
    buffSaveToken(&tokenString, tokenType, &Error);
    if(Error != SUCCESS) return INTERNAL_ERROR;
    return expr(SEMICOLON, variable->data->dataType, variable->data);
  }
}

/**
* Simulace neterminalu <TERMS>
* @param funcPtr Odkaz na funkci v TS, kterou voláme, nebo NULL, pokud jsou termy použity v příkazu write()
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int terms(tableItem* funcPtr, int paramPos){
  int Error;

  /// simulace pravidla 39: <TERMS> -> id <TERMS_N> ///
  if((tokenType = getToken(&tokenString)) == ID){ /// id ///

    /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

    // ověříme, jestli je proměnná do které načítáme vstup definovaná v daném kontextu,
    // pokud proměnná není definována vrátíme sémantickou chybu 3
    if(!variableExists(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D);

    // podle kontextu proměnné ji vytáhneme z příslušné TS
    table *TS = (getContextOfVariable(&tokenString) == CONTEXT_GLOBAL) ? &globalTS : localTS;
    // nalezneme v TS odkaz na proměnnou
    tableItem* variable = searchItem(TS, &tokenString, VARIABLE, &Error);

    // pokud jsou termy použity při volání funkce, zkontrolujeme, jestli jsou stejné jako parametry funkce z hlediska počtu a typu
    if(funcPtr != NULL){
      // pokud se termy neshodují s parametry, vracíme sémantickou chybu 4
      if(strLen(funcPtr->data->paramsTypes) < (paramPos + 1)
         || variable->data->dataType != getDataTypeIntFromChar(funcPtr->data->paramsTypes->str[paramPos])){

        HANDLE_PARSER_ERRORS(ESEM_M);
      }

      /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

      if(vestFuncCall == true){ // pokud předáváme skutečný parametr při volání vestavěné funkce
        // generujeme instrukci, která předá proměnnou jako parametr vestavěné funkci
        generateInstruction(I_SET_PAR_VEST, (void*) variable->data, (void*) NULL, (void*) NULL);
      }
      else{ // pokud předáváme parametr námi definované funkci
        string* paramName = getParamOnPosition(funcPtr, paramPos);
        tableItem* param = searchItem(funcPtr->data->localTS, paramName, VARIABLE, &Error);

        // generujeme instrukci, která předá proměnnou jako parametr funkci
        generateInstruction(I_SET_PAR, (void*) variable->data, (void*) NULL, (void*) param->data);
      }

      /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

    }
    else if (funcPtr == NULL){ // pokud je ukazatel na funkci NULL v TS, znamená to, že term slouží pro výpis ve funkci write
      // generujeme instrukci, která vypíše na stdout daný term
      generateInstruction(I_WRITE, (void*) variable->data, (void*) NULL, (void*) NULL);
    }

    /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

    return termsN(funcPtr, ++paramPos); /// <TERMS_N> ///
  }

  /// simulace pravidla 40: <TERMS> -> lit <TERMS_N> ///
  else if(tokenType == LIT_INTEGER || tokenType == LIT_REAL || tokenType == LIT_STRING || tokenType == KW_TRUE || tokenType == KW_FALSE){ /// lit ///

    /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

    // literál uloříme do globální tabulky symbolů jako proměnnou a přiřadíme jí hodnotu literálu
    string variable;
    strInit(&variable);
    generateVariable(&variable);
    tableItem* litItem = saveItem(&globalTS, &variable, VARIABLE, &Error);
    if (Error != SUCCESS) return INTERNAL_ERROR;

    switch(tokenType){
    case LIT_INTEGER:
      litItem->data->dataType = DT_INTEGER;
      litItem->data->value.intVal = atoi(tokenString.str);
      break;
    case LIT_REAL:
      litItem->data->dataType = DT_REAL;
      litItem->data->value.realVal = atof(tokenString.str);
      break;
    case LIT_STRING:
      litItem->data->dataType = DT_STRING;
      strInit(&(litItem->data->value.strVal));
      strCpy(&(litItem->data->value.strVal), &tokenString);
      break;
    case KW_TRUE:
      litItem->data->dataType = DT_BOOLEAN;
      litItem->data->value.boolVal = true;
      break;
    case KW_FALSE:
      litItem->data->dataType = DT_BOOLEAN;
      litItem->data->value.boolVal = false;
      break;
    }
    litItem->data->initialized = true; // literál vyjadřuje nějakou hodnotu, takže proměnnou, která vznikla z literálu můžeme nastavit jako inicializovanou

    // pokud jsou termy použity při volání funkce, zkontrolujeme, jestli jsou stejné jako parametry funkce z hlediska počtu a typu
    if(funcPtr != NULL){
      // pokud se termy neshodují s parametry, vracíme sémantickou chybu 4
      if(strLen(funcPtr->data->paramsTypes) < (paramPos + 1)
         || getDataTypeIntFromLit(tokenType) != getDataTypeIntFromChar(funcPtr->data->paramsTypes->str[paramPos])){

        HANDLE_PARSER_ERRORS(ESEM_M);
      }

      /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

      if (vestFuncCall == true){ // pokud je parametr předán vestavěné funkci
        // generujeme instrukci, která předá proměnnou jako parametr vestavěné funkci
        generateInstruction(I_SET_PAR_VEST, (void*) litItem->data, (void*) NULL, (void*) NULL);
      }
      else{
        string* paramName = getParamOnPosition(funcPtr, paramPos);
        tableItem* param = searchItem(funcPtr->data->localTS, paramName, VARIABLE, &Error);

        // generujeme instrukci, která předá proměnnou jako parametr
        generateInstruction(I_SET_PAR, (void*) litItem->data, (void*) NULL, (void*) param->data);
      }

      /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

    }
    else if (funcPtr == NULL){ // pokud je ukazatel na funkci NULL v TS, znamená to, že term (literál) slouží pro výpis ve funkci write
      // generujeme instrukci, která vypíše na stdout daný literál
      generateInstruction(I_WRITE, (void*) litItem->data, (void*) NULL, (void*) NULL);
    }

    /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

    return termsN(funcPtr, ++paramPos); /// <TERMS_N> ///
  }

  /// simulace pravidla 41: <TERMS> -> eps ///
  else if(tokenType == BRACKET_R){ /// eps ///

    /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

    if(funcPtr != NULL){ // pokud jsou termy použity při volání funkce, zkontrolujeme, jestli jich je stejně jako parametrů funkce
      // pokud není stejný počet termů a parametrů volané funkce, vracíme sémantickou chybu 4
      if(strLen(funcPtr->data->paramsTypes) != paramPos) HANDLE_PARSER_ERRORS(ESEM_M);
    }

    /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

    return EOK;
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <TERMS_N>
* @param funcPtr Odkaz na funkci v TS, kterou voláme, nebo NULL, pokud jsou termy použity v příkazu write()
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int termsN(tableItem* funcPtr, int paramPos){
  int Error;

  /// simulace pravidla 42: <TERMS-N> -> , id <TERMS_N>  ///
  ///                   48: <TERMS-N> -> , lit <TERMS_N> ///
  if((tokenType = getToken(&tokenString)) == COMA){ /// , ///

    /// simulace pravidla 42: <TERMS> -> , id <TERMS_N> ///
    if((tokenType = getToken(&tokenString)) == ID){ /// id ///

      /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

      // ověříme, jestli je proměnná do které načítáme vstup definovaná v daném kontextu,
      // pokud proměnná není definována vrátíme sémantickou chybu 3
      if(!variableExists(&tokenString)) HANDLE_PARSER_ERRORS(ESEM_D)

      // podle kontextu proměnné ji vytáhneme z příslušné TS
      table *TS = (getContextOfVariable(&tokenString) == CONTEXT_GLOBAL) ? &globalTS : localTS;
      // nalezneme v TS odkaz na proměnnou
      tableItem* variable = searchItem(TS, &tokenString, VARIABLE, &Error);

      // pokud jsou termy použity při volání funkce, zkontrolujeme, jestli jsou stejné jako parametry funkce z hlediska počtu a typu
      if(funcPtr != NULL){
        // pokud se termy neshodují s parametry, vracíme sémantickou chybu 4
        if(strLen(funcPtr->data->paramsTypes) < (paramPos + 1)
           || variable->data->dataType != getDataTypeIntFromChar(funcPtr->data->paramsTypes->str[paramPos])){

          HANDLE_PARSER_ERRORS(ESEM_M);
        }

        /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

        if(vestFuncCall == true){ // pokud předáváme skutečný parametr při volání vestavěné funkce
          // generujeme instrukci, která předá proměnnou jako parametr vestavěné funkci
          generateInstruction(I_SET_PAR_VEST, (void*) variable->data, (void*) NULL, (void*) NULL);
        }
        else{ // pokud předáváme parametr námi definované funkci
          string* paramName = getParamOnPosition(funcPtr, paramPos);
          tableItem* param = searchItem(funcPtr->data->localTS, paramName, VARIABLE, &Error);

          // generujeme instrukci, která předá proměnnou jako parametr funkci
          generateInstruction(I_SET_PAR, (void*) variable->data, (void*) NULL, (void*) param->data);
        }

        /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

      }
      else if (funcPtr == NULL){ // pokud je ukazatel na funkci NULL v TS, znamená to, že term slouží pro výpis ve funkci write
        // generujeme instrukci, která vypíše na stdout daný term
        generateInstruction(I_WRITE, (void*) variable->data, (void*) NULL, (void*) NULL);
      }

      /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

      return termsN(funcPtr, ++paramPos); /// <TERMS_N> ///
    }

    /// simulace pravidla 48: <TERMS-N> -> , lit <TERMS_N> ///
    else if(tokenType == LIT_INTEGER || tokenType == LIT_REAL || tokenType == LIT_STRING || tokenType == KW_TRUE || tokenType == KW_FALSE){ /// lit ///

      /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

      // pokud je skuztečný parametr literál, uložíme jej do globální tabulky symbolů jako proměnnou a přiřadíme jí hodnotu literáru
      string variable;
      strInit(&variable);
      generateVariable(&variable);
      tableItem* litItem = saveItem(&globalTS, &variable, VARIABLE, &Error);
      if (Error != SUCCESS) return INTERNAL_ERROR;

      switch(tokenType){
      case LIT_INTEGER:
        litItem->data->dataType = DT_INTEGER;
        litItem->data->value.intVal = atoi(tokenString.str);
        break;
      case LIT_REAL:
        litItem->data->dataType = DT_REAL;
        litItem->data->value.realVal = atof(tokenString.str);
        break;
      case LIT_STRING:
        litItem->data->dataType = DT_STRING;
        strInit(&(litItem->data->value.strVal));
        strCpy(&(litItem->data->value.strVal), &tokenString);
        break;
      case KW_TRUE:
        litItem->data->dataType = DT_BOOLEAN;
        litItem->data->value.boolVal = true;
        break;
      case KW_FALSE:
        litItem->data->dataType = DT_BOOLEAN;
        litItem->data->value.boolVal = false;
        break;
      }
      litItem->data->initialized = true;  // literál vyjadřuje nějakou hodnotu, takže proměnnou, která vznikla z literálu můžeme nastavit jako inicializovanou

      // pokud jsou termy použity při volání funkce, zkontrolujeme, jestli jsou stejné jako parametry funkce z hlediska počtu a typu
      if(funcPtr != NULL){
        // pokud se termy neshodují s parametry, vracíme sémantickou chybu 4
        if(strLen(funcPtr->data->paramsTypes) < (paramPos + 1)
           || getDataTypeIntFromLit(tokenType) != getDataTypeIntFromChar(funcPtr->data->paramsTypes->str[paramPos])){

          HANDLE_PARSER_ERRORS(ESEM_M);
        }

        /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

        if (vestFuncCall == true){ // pokud je parametr předán vestavěné funkci
          // generujeme instrukci, která předá proměnnou jako parametr vestavěné funkci
          generateInstruction(I_SET_PAR_VEST, (void*) litItem->data, (void*) NULL, (void*) NULL);
        }
        else{
          string* paramName = getParamOnPosition(funcPtr, paramPos);
          tableItem* param = searchItem(funcPtr->data->localTS, paramName, VARIABLE, &Error);

          // generujeme instrukci, která předá proměnnou jako parametr
          generateInstruction(I_SET_PAR, (void*) litItem->data, (void*) NULL, (void*) param->data);
        }

        /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

      }
      else if (funcPtr == NULL){ // pokud je ukazatel na funkci NULL v TS, znamená to, že term (literál) slouží pro výpis ve funkci write
        // generujeme instrukci, která vypíše na stdout daný literál
        generateInstruction(I_WRITE, (void*) litItem->data, (void*) NULL, (void*) NULL);
      }

      /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

      return termsN(funcPtr, ++paramPos); /// <TERMS_N> ///
    }
    else if(invalidToken(tokenType)) return tokenType;
    else HANDLE_PARSER_ERRORS(EPARS);

  }

  /// simulace pravidla 43: <TERMS-N> -> eps ///
  else if(tokenType == BRACKET_R){ /// eps ///

    /* *************************************** ZAČÁTEK SÉMANTICKÝCH AKCÍ *************************************** */

    if(funcPtr != NULL){ // pokud jsou termy použity při volání funkce, zkontrolujeme, jestli jich je stejně jako parametrů funkce
      // pokud není stejný počet termů a parametrů volané funkce, vracíme sémantickou chybu 4
      if(strLen(funcPtr->data->paramsTypes) != paramPos) HANDLE_PARSER_ERRORS(ESEM_M);
    }

    /* *************************************** KONEC SÉMANTICKÝCH AKCÍ *************************************** */

    return EOK;
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <BODY_CONTINUE>
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int bodyContinue(){
  /// simulace pravidla 44: <BODY_CONTINUR> -> ; <BODY_NON_EPS> ///

  // token ; uz mame vzdycky nacteny, jen jej musime zkontrolovat
  if(tokenType == SEMICOLON){ /// ; ///
    return bodyNonEps(); /// <BODY_NON_EPS> ///
  }
  /// simulace pravidla 45: <BODY_CONTINUR> -> eps ///
  else if(tokenType == KW_END){ /// eps ///

    /* *************************************** ZAČÁTEK GENEROVÁNÍ KÓDU *************************************** */

    // generujeme instrukci, která nedelá nic, ale je v instrukčním listu, aby zabránila některým problémům
    generateInstruction(I_NOP, (void*) NULL, (void*) NULL, (void*) NULL);

    /* *************************************** KONEC GENEROVÁNÍ KÓDU *************************************** */

    return EOK;
  }
  else if(invalidToken(tokenType)) return tokenType;
  else HANDLE_PARSER_ERRORS(EPARS);
}

/**
* Simulace neterminalu <EXPR>
* @param breakpoint Zarazka, kterou kdyz nacte <EXPR> tak skonci
* @return  int     Navratovy kod z vyctu "tecodes"
*/
int expr(int breakpoint, int expectedDataType, Data *variableData){
  int retCode;

  // zde pro zpracování výraz zavoláme funkci pro precedenční syntaktickou analýzu
  retCode = precedenceAnalyze(localTS, &globalTS, context, expectedDataType, &tokenType, variableData, NULL);

  if (retCode != EOK) HANDLE_PARSER_ERRORS(retCode);
}

/* ************************************************************************************************************************ */
/* ****************************************** POMOCNÉ FUNKCE PRO PRÁCI S TOKENY ******************************************* */
/* ************************************************************************************************************************ */


/**
* Funkce, která ověřuje validitu tokenu, tedy jestli nám lexikální analýzátor vrátil validní token, nebo nějakou chybu
* @param tokenType Typ tokenu, pokud je záporné číslo, je token nevalidní
* @return bool Vrací true nebo false, v závislosti na tom, jestli je token validní
*/
bool invalidToken(int tokenType){
  if (tokenType < 0){
    return true;
  }
  else return false;
}

/**
* Funkce, která nám vrací z bufferu poslední token - chová se jako fronta
* @param tokenString Ukazatel na token
* @return int Typ tokenu jako celočíselná konstanta
*/
int buffGetToken(string *token){

    // pokud v bufferu není žádný prvek, nebudeme z něj nic načítat, ale budeme volat přímo getToken()
    if(bufferOfTokens[0] == NULL && bufferOfTokens[1] == NULL){
      tokenType = getToken(token);
    }
    // pokud je na první pozici token, vrátíme jej
    else if(bufferOfTokens[0] != NULL){
      if(strCpy(token, bufferOfTokens[0]->tokenString) != STR_SUCCESS) return INTERNAL_ERROR;
      token->length = bufferOfTokens[0]->tokenString->length;
      token->allocSize = bufferOfTokens[0]->tokenString->allocSize;

      tokenType = bufferOfTokens[0]->tokenType;
      buffDeleteOnIndex(0); // a na daném indexu token odstraníme
    }
    // pokud je na druhé pozici token, vrátíme jej
    else if(bufferOfTokens[1] != NULL){
      if(strCpy(token, bufferOfTokens[1]->tokenString) != STR_SUCCESS) return INTERNAL_ERROR;
      token->length = bufferOfTokens[1]->tokenString->length;
      token->allocSize = bufferOfTokens[1]->tokenString->allocSize;

      tokenType = bufferOfTokens[1]->tokenType;
      buffDeleteOnIndex(1); // a na daném indexu token odstraníme
    }

    return tokenType;
}

/**
* Funkce, která nám do bufferu uloží token - chová se jako fronta
* @param tokenString Ukazatel na token
* @param tokenType Typ tokenu
* @param Error Pomocí tohoto parametru nastavujeme příznaky chyby
* @return void
*/
void buffSaveToken(string *tokenString, int tokenType, int *Error){

  // alokujeme prostor pro nový token
  tokenData *newToken = malloc(sizeof(struct tokenData));
  if(newToken == NULL){
    *Error = INTERNAL_ERROR;
    return;
  }

  // alokujeme prostor pro název tokenu
  string *newTokenString = malloc(sizeof(string));
  if(newTokenString == NULL){
    *Error = INTERNAL_ERROR;
    return;
  }
  if(strInit(newTokenString) != STR_SUCCESS){
    *Error = INTERNAL_ERROR;
    return;
  }
  if(strCpy(newTokenString, tokenString)){
    *Error = INTERNAL_ERROR;
    return;
  }
  // zkopírujeme současný načtený název tokenu do nově vytvořeného názvu tokenu
  if(strCpy(newTokenString, tokenString) != STR_SUCCESS){
    *Error = INTERNAL_ERROR;
    return;
  }

  // inicializujeme token
  newToken->tokenString = newTokenString;
  newToken->tokenType = tokenType;

  // uložíme token na první volnou pozici
  if(bufferOfTokens[0] == NULL){
    bufferOfTokens[0] = newToken;
  }
  else{
    bufferOfTokens[1] = newToken;
  }

  *Error = SUCCESS;
}

/**
* Funkce, která na daném indexu smaže token v bufferu
* @param index Index na kterém se nachází token určený ke smazání
* @return void
*/
void buffDeleteOnIndex(int index){
  strFree(bufferOfTokens[index]->tokenString);
  free(bufferOfTokens[index]->tokenString);
  free(bufferOfTokens[index]);
  bufferOfTokens[index] = NULL;
}

/**
* Funkce, která nám vyprázdní celý buffer tokenů
* @return void
*/
void buffTruncate(){
  if(bufferOfTokens[0] != NULL){
    strFree(bufferOfTokens[0]->tokenString);
    free(bufferOfTokens[0]->tokenString);
    free(bufferOfTokens[0]);
    bufferOfTokens[0] = NULL;
  }
  else if(bufferOfTokens[1] != NULL){
    strFree(bufferOfTokens[1]->tokenString);
    free(bufferOfTokens[1]->tokenString);
    free(bufferOfTokens[1]);
    bufferOfTokens[1] = NULL;
  }
}

/* ************************************************************************************************************************ */
/* **************************************** ZAČÁTEK FUNKCÍ PRO SÉMANTICKOU ANALÝZU **************************************** */
/* ************************************************************************************************************************ */

/**
* Funkce která naplní globální tabulku symbolů vestavěnými funkcemi
* @param Error Ukazatel pomocí kterého budeme vnějšímu okolí vrace případné chyby
* @return void
*/
void fillGlobalTSbyBuildInFunctions(int *Error){
  string vestFunction;
  string vestFunctionParamType;
  string vestFunctionParam;

  // vložíme funkci length
  vestFunction.str = "LENGTH";
  vestFunction.length = strlen(vestFunction.str);
  vestFunction.allocSize = strlen(vestFunction.str);
  tableItem *item = saveItem(&globalTS, &vestFunction, FUNCTION, Error);
  if(*Error != SUCCESS) return;

  item->data->dataType = DT_INTEGER;
  item->data->declared = true;
  item->data->defined = true;

  vestFunctionParamType.str = "s";
  vestFunctionParamType.length = strlen(vestFunctionParamType.str);
  vestFunctionParamType.allocSize = strlen(vestFunctionParamType.str);

  if(strCpy(item->data->paramsTypes, &vestFunctionParamType)){
      *Error = INTERNAL_ERROR;
      return;
  }

  vestFunctionParam.str = "S";
  vestFunctionParam.length = strlen(vestFunctionParam.str);
  vestFunctionParam.allocSize = strlen(vestFunctionParam.str);
  addNextFuncParam(item, 0, &vestFunctionParam, Error);
  if(*Error != SUCCESS) return INTERNAL_ERROR;

  //vložíme funkci copy
  vestFunction.str = "COPY";
  vestFunction.length = strlen(vestFunction.str);
  vestFunction.allocSize = strlen(vestFunction.str);
  tableItem *item2 = saveItem(&globalTS, &vestFunction, FUNCTION, Error);
  if(*Error != SUCCESS) return;

  item2->data->dataType = DT_STRING;
  item2->data->declared = true;
  item2->data->defined = true;

  vestFunctionParamType.str = "sii";
  vestFunctionParamType.length = strlen(vestFunctionParamType.str);
  vestFunctionParamType.allocSize = strlen(vestFunctionParamType.str);

  if(strCpy(item2->data->paramsTypes, &vestFunctionParamType)){
      *Error = INTERNAL_ERROR;
      return;
  }

  vestFunctionParam.str = "S";
  vestFunctionParam.length = strlen(vestFunctionParam.str);
  vestFunctionParam.allocSize = strlen(vestFunctionParam.str);
  addNextFuncParam(item2, 0, &vestFunctionParam, Error);
  if(*Error != SUCCESS) return INTERNAL_ERROR;

  vestFunctionParam.str = "I";
  vestFunctionParam.length = strlen(vestFunctionParam.str);
  vestFunctionParam.allocSize = strlen(vestFunctionParam.str);
  addNextFuncParam(item2, 1, &vestFunctionParam, Error);
  if(*Error != SUCCESS) return INTERNAL_ERROR;

  vestFunctionParam.str = "N";
  vestFunctionParam.length = strlen(vestFunctionParam.str);
  vestFunctionParam.allocSize = strlen(vestFunctionParam.str);
  addNextFuncParam(item2, 2, &vestFunctionParam, Error);
  if(*Error != SUCCESS) return INTERNAL_ERROR;

  // vložíme funkci find
  vestFunction.str = "FIND";
  vestFunction.length = strlen(vestFunction.str);
  vestFunction.allocSize = strlen(vestFunction.str);
  tableItem *item3 = saveItem(&globalTS, &vestFunction, FUNCTION, Error);
  if(*Error != SUCCESS) return;

  item3->data->dataType = DT_INTEGER;
  item3->data->declared = true;
  item3->data->defined = true;

  vestFunctionParamType.str = "ss";
  vestFunctionParamType.length = strlen(vestFunctionParamType.str);
  vestFunctionParamType.allocSize = strlen(vestFunctionParamType.str);

  if(strCpy(item3->data->paramsTypes, &vestFunctionParamType)){
      *Error = INTERNAL_ERROR;
      return;
  }

  vestFunctionParam.str = "S";
  vestFunctionParam.length = strlen(vestFunctionParam.str);
  vestFunctionParam.allocSize = strlen(vestFunctionParam.str);
  addNextFuncParam(item3, 0, &vestFunctionParam, Error);
  if(*Error != SUCCESS) return INTERNAL_ERROR;

  vestFunctionParam.str = "SEARCH";
  vestFunctionParam.length = strlen(vestFunctionParam.str);
  vestFunctionParam.allocSize = strlen(vestFunctionParam.str);
  addNextFuncParam(item3, 1, &vestFunctionParam, Error);
  if(*Error != SUCCESS) return INTERNAL_ERROR;

    // vložíme funkci sort
  vestFunction.str = "SORT";
  vestFunction.length = strlen(vestFunction.str);
  vestFunction.allocSize = strlen(vestFunction.str);
  tableItem *item4 = saveItem(&globalTS, &vestFunction, FUNCTION, Error);
  if(*Error != SUCCESS) return;

  item4->data->dataType = DT_STRING;
  item4->data->declared = true;
  item4->data->defined = true;

  vestFunctionParamType.str = "s";
  vestFunctionParamType.length = strlen(vestFunctionParamType.str);
  vestFunctionParamType.allocSize = strlen(vestFunctionParamType.str);

  if(strCpy(item4->data->paramsTypes, &vestFunctionParamType)){
      *Error = INTERNAL_ERROR;
      return;
  }

  vestFunctionParam.str = "S";
  vestFunctionParam.length = strlen(vestFunctionParam.str);
  vestFunctionParam.allocSize = strlen(vestFunctionParam.str);
  addNextFuncParam(item4, 0, &vestFunctionParam, Error);
  if(*Error != SUCCESS) return INTERNAL_ERROR;


}

/**
* Funkce která ověřuje jestli byla proměnná deklarována v současném kontextu
* @param tokenString Token, který reprezentuje název proměnné
* @return true Daná proměnná byla v současném kontextu deklarována
* @return false Daná proměnná nebyla v současném kontextu deklarována
*/
bool isVariableDeclared(string *tokenString){
  int Error;
  // v závislosti na kontextu budeme hledat proměnnou v globální, nebo lokální tabulce symbolů
  if(context == CONTEXT_GLOBAL){
    searchItem(&globalTS, tokenString, VARIABLE, &Error);
  }
  else if(context == CONTEXT_LOCAL){
    searchItem(localTS, tokenString, VARIABLE, &Error);
  }
  // pokud jsem proměnnou nalezli vracíme true, jinak vracíme false
  if (Error == SUCCESS) return true;
  else return false;
}

/**
* Funkce která ověřuje existenci funkce na základě jejího identifikátou
* @param tokenString Token, který reprezentuje název funkce
* @return true Daná funkce existuje
* @return false Daná funkce neexistuje
*/
bool functionExists(string *tokenString){
  int Error;
  // budeme hledat funkci v globální tabulce symbolů
  searchItem(&globalTS, tokenString, FUNCTION, &Error);
  // pokud jsem funkci nalezli vracíme true
  if (Error == SUCCESS) return true;
  else return false;
}

/**
* Funkce která zjišťuje, jestli byla pro každou dopřednou deklaraci nalezena, také dopředná definice funkce
* @return true Pro každou dopřednou deklaraci byla nalezena odpovídající definice funkce
* @return false Pro některou dopřednou deklaraci nebyla nalezena odpovídající definice funkce
*/
bool forEachForwDecExistFuncDef(){
  tableItem *item; // ukazovátko na položku v tabulce symbolů
  // prohledáme všechny funkce v TS a ověříme jestli byly deklarované funkce i definovány
  for(int i = 0; i < TABLESIZE; i++){
    if (globalTS[i] != NULL){ // pokud na daném indexu TS existuje alespoň jeden prvek
      item = globalTS[i];
      // prohledáváme zřetězený seznam, dokud se nedostanem na jeho konec, nebo nalezenem alespoň jednu funkci,
      // která je dopředně deklarována, ale už ne definována
      do{
        if(item->data->identifierType == FUNCTION){
          if (item->data->declared == true && item->data->defined == false){
            return false;
          }
        }
      } while((item = item->next) != NULL);
    }
  }

  return true;
}

/**
* Funkce přidá k funkci do TS další název parametru
* @param funcPtr Ukazatel na funci v globální tabulce symbolů
* @param paramPos Pozice parametru (indexujeme od 0)
* @param tokenString Token který představuje název parametru
* @param Error Ukazatel na proměnnou Error, do které uložíme chybu, pokud nastala, nebo příznak, že chyba nenastala
* @return void
*/
void addNextFuncParam(tableItem *funcPtr, int paramPos, string *tokenString, int *Error){
  // alokujeme strukturu, která bude představovat název parametru
  ParamsNames *paramsNames = malloc(sizeof(struct ParamsNames));
  if(paramsNames == NULL){
    *Error = INTERNAL_ERROR;
    return;
  }

  // vytvoříme prostor pro název parametru a uložíme do něj novou hodnotu
  string *paramName = malloc(sizeof(string));
  if(paramName == NULL){
    *Error = INTERNAL_ERROR;
    return;
  }
  if(strInit(paramName) != STR_SUCCESS){
    *Error = INTERNAL_ERROR;
    return;
  }
  if(strCpy(paramName, tokenString) != STR_SUCCESS){
    *Error = INTERNAL_ERROR;
    return;
  }

  // daty naplníme strukturu, která bude představovat název parametru
  paramsNames->next = NULL;
  paramsNames->index = paramPos;
  paramsNames->paramName = paramName;

  if(funcPtr->data->paramsNames == NULL){ // pokud ukládáme první parametr
    // navážeme seznam zřetězených názvů parametrů (čítající zatím jednu položku) na funkci v TS
    funcPtr->data->paramsNames = paramsNames;
  }
  else{ // pokud ukládáme i-tý parametr, kde i > 1
    ParamsNames **tmpParamsNamesPtrPtr = &(funcPtr->data->paramsNames); // ukazovátko na parametry funkce
    do{ // projedeme zeřtězeným seznamem názvů parametrů a za poslední prvek připojíme ukládaný parametr
      if((*tmpParamsNamesPtrPtr)->next == NULL){
        (*tmpParamsNamesPtrPtr)->next = paramsNames;
        break;
      }
    } while(*(tmpParamsNamesPtrPtr = &((*tmpParamsNamesPtrPtr)->next)) != NULL);
  }
  *Error = SUCCESS;

  return;
}

/**
* Funkce ověří, jestli je název parametru v definici funkce shodný s odpovídajícím názvem parametru v dopředné deklaraci
* @param funcPtr Ukazatel na funci v globální tabulce symbolů
* @param paramPos Pozice parametru (indexujeme od 0)
* @param tokenString Token který představuje název parametru
* @return true Pokud mají parametry na dané pozici stejný název
* @return false Pokud nemají parametry na dané pozici stejný název
*/
bool paramNamesAreEqual(tableItem *funcPtr, int paramPos, string *tokenString){
  ParamsNames *paramsNamesPtr = funcPtr->data->paramsNames; // ukazovátko na současný a následně také na další parametr
  do{ // postupně budeme procházet parametry funkce
    // pokud na dané pozici nalezneme shodný název parametru, tak si parametry z hlediska názvu odpovídají
    if(paramsNamesPtr->index == paramPos && strCmp(paramsNamesPtr->paramName, tokenString) == 0){
      return true;
    }
  } while((paramsNamesPtr = paramsNamesPtr->next) != NULL);

  return false;
}

/**
* Funkce pro daný index parametru vrátí jeho název
* @param funcPtr Ukazatel na funci v globální tabulce symbolů
* @param paramPos Pozice parametru (indexujeme od 0)
* @return string Ukazatel na název parametru
*/
string* getParamOnPosition(tableItem *funcPtr, int paramPos){
  ParamsNames *paramsNamesPtr = funcPtr->data->paramsNames; // ukazovátko na současný a následně také na další parametr
  do{ // postupně budeme procházet parametry funkce
    // pro danou pozici nalezneme název parametru
    if(paramsNamesPtr->index == paramPos){
      return paramsNamesPtr->paramName;
    }
  } while((paramsNamesPtr = paramsNamesPtr->next) != NULL);

  return NULL;
}

/**
* Funkce, ověří, jestli se název proměnné, reprezentován tokenem, nachází v globální nebo lokální tabulce symbolů,
* jinými slovy, jestli proměnná s daným názvem je už definovaná
* @param tokenString Token který představuje název proměnné
* @return true Pokud je proměnná definována
* @return false Pokud proměnná není definována
*/
bool variableExists(string *tokenString){
  int Error;
  // v závislosti na kontextu budeme hledat proměnnou v globální, nebo lokální tabulce symbolů
  if(context == CONTEXT_GLOBAL){
    searchItem(&globalTS, tokenString, VARIABLE, &Error);
  }
  else if(context == CONTEXT_LOCAL){
    searchItem(localTS, tokenString, VARIABLE, &Error);
    if(Error == NOT_FOUND){ // pokud jsme proměnnou nenalezli v lokálním kontextu, zkusíme ještě prohledat globální konext
      searchItem(&globalTS, tokenString, VARIABLE, &Error);
    }
  }
  // pokud jsem proměnnou nalezli vracíme true, jinak vracíme false
  if (Error == SUCCESS) return true;
  else return false;
}

/**
* Funkce, která vrátí kontext (lokální, globální) ve kterém se vyskytuje název proměnné předaná parametrem
* Před zavoláním této funkce musíme ověřit, že proměnná s daným název skutečně existuje a to pomocí funkce variableExists
* @param tokenString Token který představuje název proměnné
* @return int Typ kontextu (vyjádřený celočíselnou konstantou), kde se proměnná vyskytuje
*/
int getContextOfVariable(string *tokenString){
  int Error;

  if(context == CONTEXT_GLOBAL){ // pokud jsme v globálním kontextu, tak proměnná bude také v globálním kontextu
    return CONTEXT_GLOBAL;
  }
  else if(context == CONTEXT_LOCAL){ // pokud jsme v lokálním kontextu, proměnná může být v lokálním, ale i globálním kontextu
    searchItem(localTS, tokenString, VARIABLE, &Error);
    if(Error == NOT_FOUND){ // pokud proměnná není v lokálním kontextu, tak bude v globálním kontextu
      return CONTEXT_GLOBAL;
    }
    else return CONTEXT_LOCAL;
  }
}

/**
* Funkce, která překonvertuje datový typ vyjádřený pomocí znaku na datový typ reprezentován celočíselnou konstantou
* @param dataType Datový typ
* @return int Datový typ
*/
int getDataTypeIntFromChar(char dataType){
  switch(dataType){
    case 'i':
      return DT_INTEGER;
    case 'r':
      return DT_REAL;
    case 's':
      return DT_STRING;
    case 'b':
      return DT_BOOLEAN;
    }
}

/**
* Funkce, která překonvertuje typ literálu (reprezentován celočíselnou konstantou)
* na datový typ (reprezentován celočíselnou konstantou)
* @param litType Typ literálu
* @return int Datový typ
*/
int getDataTypeIntFromLit(int litType){
  switch(litType){
    case LIT_INTEGER:
      return DT_INTEGER;
    case LIT_REAL:
      return DT_REAL;
    case LIT_STRING:
      return DT_STRING;
    case KW_TRUE:
    case KW_FALSE:
      return DT_BOOLEAN;
    }
}

/**
* Funkce, která smaže všechny lokální tabulky symbolů
* @return void
*/
void deleteAllGlobalTables(){
  tableItem *item; // ukazovátko na položku v tabulce symbolů
  // prohledáme všechny funkce v TS a smažeme jejich TS
  for(int i = 0; i < TABLESIZE; i++){
    if (globalTS[i] != NULL){ // pokud na daném indexu TS existuje alespoň jeden prvek
      item = globalTS[i];
      // prohledáváme zřetězený seznam, dokud se nedostanem na jeho konec
      do{
        if(item->data->identifierType == FUNCTION){
          deleteTable(item->data->localTS);
          free(item->data->localTS); // uvolníme takovouto lokální tabulku
        }
      } while((item = item->next) != NULL);
    }
  }

  return true;
}

/* ************************************************************************************************************************ */
/* ************************************* FUNKCE VYUŽITÉ PŘI GENEROVÁNÍ VNITŘNÍHO KÓDU ************************************* */
/* ************************************************************************************************************************ */

/**
* Funkce generuje jedinecne nazvy identifikatoru
* nazev se sklada ze znaku $ nasledovanym cislem
* postupne se tu generuji prirozena cisla a do nazvu promenne se ukladaji
* v reverzovanem poradi - na funkcnost to nema vliv, ale je jednodussi implementace
* @param var Ukazatel na mallocnutý a inilializovaný řetězec
* @return void
*/
void generateVariable(string *var){
  strClr(var);
  strCat(var, '$');
  int i;
  i = counterVar;
  while (i != 0)
  {
    strCat(var, (char)(i % 10 + '0'));
    i = i / 10;
  }
  counterVar ++;
}

/**
* Funkce vlozi novou instrukci do seznamu instrukci
* @param instType Typ instrukce vyjádřený celočíselnou konstantou
* @param instType addr1 Adresa prvního operandu
* @param instType addr2 Adresa druhého operandu
* @param instType addr3 Adresa výsledku
* @return void
*/
void generateInstruction(int instType, void *addr1, void *addr2, void *addr3){

  tInstr I;
  I.instType = instType;
  I.addr1 = addr1;
  I.addr2 = addr2;
  I.addr3 = addr3;
  listInsertLast(&list, I); // uložíme instrukci do instrukčního listu

  if (isFirstInstructionInFunc == true){              // pokud se jedná o první instrukci ve funkci
    currentFunction->firstInstructInFunc = list.last; // nastavíme pro danou funkci v TS její první instrukci
    isFirstInstructionInFunc = false;
  }
  if(lastKeywordWasElse == true){                               // pokud bylo poslední klíčoví slovo else a poslední uložená instrukce tak byla první instrukcí v tomto bloku
    if(instructEmpty(&ifInstructStack) == false){               // pokud na zásobníku instrukcí I_IF něco je, popneme to
      tListItem *lastIfInstruc = instructPop(&ifInstructStack); // popneme si ukazatel na poslední if instrukci ze zásobníku if instrukcí
      lastIfInstruc->Instruction.addr3 = (void*) list.last;     // v poslední instrukci I_IF nasvíme třetí adresu na poslední vloženou instrukci
    }
    lastKeywordWasElse = false;                                 // příznak vynulujeme
  }
  if(lastInstructWasAfterBlockElse == true){                        // byla poslední uložená instrukce první instrukcí za blokem else
    if(instructEmpty(&elseInstructStack) == false){                 // pokud na zásobníku instrukcí I_ELSE něco je, popneme to
      tListItem *lastElseInstruc = instructPop(&elseInstructStack); // popneme si ukazatel na poslední I_ELSE instrukci ze zásobníku else instrukcí
      lastElseInstruc->Instruction.addr3 = (void*) list.last;       // v poslední instrukci I_ELSE nasvíme třetí adresu na poslední vloženou instrukci
    }
    lastInstructWasAfterBlockElse = false;                          // příznak vynulujeme
  }
  if(lastInstructWasAfterBlockWhile == true){                          // pokud poslední instrukce v instrukčním listu je první instrukcí za blokem while
    if(instructEmpty(&whileInstructStack) == false){                   // pokud na zásobníku instrukcí I_WHILE něco je, popneme to
      tListItem *lastWhileInstruc1 = instructPop(&whileInstructStack); // popneme si ukazatel na poslední I_WHILE instrukci ze zásobníku while instrukcí
      lastWhileInstruc1->Instruction.addr3 = (void*) list.last;        // v poslední instrukci I_WHILE nasvíme třetí adresu na poslední vloženou instrukci
    }
    lastInstructWasAfterBlockWhile = false;                            // příznak vynulujeme
  }
  if(lastInstructWasBoot == true){  // pokud je poslední instrukce v instrukčním listu zaváděcí instrukce
    bootInstruction = list.last;    // ukazatel na zaváděcí instrukci
    lastInstructWasBoot = false;    // příznak, který určuje jestli byla poslední instrukce, bootovací instrukcí
  }
  if(lastInstructWasWhileCondition == true) { // pokud je poslední instrukce v instrukčním listu, instrukce, kterou začíná výraz ve while cyklu
    if (instructPush(&exprWhileInstructStack, list.last) != INST_SUCCESS) return INTERNAL_ERROR; // uložíme si na zásobník odkaz na výraz v odpovídajícím cyklu while
    lastInstructWasWhileCondition = false;    // příznak vynulujeme
  }

}

/**
* Funkce slouží v rámci debugování pro přehledný výpis vygenerovaných instrukcí
* @return void
*/
void vypisInstrukce(){
  tListItem* listItemm = list.first;
  char *nazevInstrukce ;

  // vytiskneme hlavičku tabulky instrukci
  printf("\n");
  fflush(stdout);
  printf(ANSI_COLOR_MAGENTA " %-30s %-11s | %-11s | %-11s || %-11s\n" ANSI_COLOR_RESET, "INSTRUCTION", "OP1 ADDR", "OP2 ADDR", "RESULT ADDR", "INSTR ADDR");
  fflush(stdout);
  printf("--------------------------------------------------------------------------------------\n");
  fflush(stdout);

  // v cyklu budeme tisknou všechny instrukce (jejich název, jejich operandy, jejich adresy)
  do{
    switch(listItemm->Instruction.instType){
    case 0:
      nazevInstrukce = "HALT";
      break;
    case 1:
      nazevInstrukce = "I_NOP";
      break;
    case 3:
      nazevInstrukce = "I_CREATE_FRAME";
      break;
    case 4:
      nazevInstrukce = "I_CALL";
      break;
    case 5:
      nazevInstrukce = "I_CALL_VEST";
      break;
    case 7:
      nazevInstrukce = "I_SET_PAR_VEST";
      break;
    case 6:
      nazevInstrukce = "I_SET_PAR";
      break;
    case 12:
      nazevInstrukce = "I_RETURN";
      break;
    case 11:
      nazevInstrukce = "I_SET_RESULT_AND_DESTROY_FRAME";
      break;
    case 13:
      nazevInstrukce = "I_ASSIGN";
      break;
    case 14:
      nazevInstrukce = "I_IF";
      break;
    case 15:
      nazevInstrukce = "I_ELSE";
      break;
    case 16:
      nazevInstrukce = "I_WHILE";
      break;
    case 17:
      nazevInstrukce = "I_REPEAT_WHILE";
      break;
    case 18:
      nazevInstrukce = "I_READLN";
      break;
    case 19:
      nazevInstrukce = "I_WRITE";
      break;
    case 21:
      nazevInstrukce = "I_ADD";
      break;
    case 22:
      nazevInstrukce = "I_STR_CONCAT";
      break;
    case 23:
      nazevInstrukce = "I_SUB";
      break;
    case 24:
      nazevInstrukce = "I_MUL";
      break;
    case 25:
      nazevInstrukce = "I_DIV";
      break;
    case 26:
      nazevInstrukce = "I_LESS";
      break;
    case 27:
      nazevInstrukce = "I_MORE";
      break;
    case 28:
      nazevInstrukce = "I_LESS_EQUAL";
      break;
    case 29:
      nazevInstrukce = "I_MORE_EQUAL";
      break;
    case 30:
      nazevInstrukce = "I_EQUAL";
      break;
    case 31:
      nazevInstrukce = "I_LESS_MORE";
      break;
    }

    // bootovací instrukci odlišíme od jiných
    if (listItemm == bootInstruction){
      printf(ANSI_COLOR_CYAN " %-30s " ANSI_COLOR_RESET "%-11p | %-11p | %-11p || %-11p\n", nazevInstrukce, listItemm->Instruction.addr1, listItemm->Instruction.addr2, listItemm->Instruction.addr3, listItemm);
      fflush(stdout);
    }
    else{
      printf(" %-30s %-11p | %-11p | %-11p || %-11p\n", nazevInstrukce, listItemm->Instruction.addr1, listItemm->Instruction.addr2, listItemm->Instruction.addr3, listItemm);
      fflush(stdout);
    }
    printf("--------------------------------------------------------------------------------------\n");
    fflush(stdout);
  } while((listItemm = listItemm->nextItem) != NULL);
}

/**
* Funkce slouží pro uvolnění použitých zdrojů v parseru
* @return void
*/
void cleanResources(){
  listFree(&list); // uvolníme seznam instrukcí

  // uvolníme paměť zabíranou tabulkami symbolů a tokenem
  deleteTable(&globalTS);
  deleteAllGlobalTables();
  strFree(&tokenString);

  // vymažeme zásobníky instrukcí
  instructStackFree(&ifInstructStack);
  instructStackFree(&elseInstructStack);
  instructStackFree(&exprWhileInstructStack);
  instructStackFree(&whileInstructStack);

  buffTruncate(); // vyprázdníme buffer tokenů
}

