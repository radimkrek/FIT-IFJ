﻿# PROJEKT IFJ 2014/2015 #
## 
1. spuštění debugu: ##

```
#!c

    make clean,
    make all,
    chmod 755 program,
    ./program soubor.testu
```

    
Důležité INFO!!!

- Ve scaneru byla přejmenována f-ce getToken na orgGetToken. a vytvořena nová funkce getToken.
A to z důvodu DEBUGU, aby sceny vždy vytisknul co za token předal.

- erorCodesMsg: MUSÍ zůstat jen v main.c  jinak to zhodí celou aplikaci 


- Testy si zatím musí dělat každý sám. Ale pokud si nějaký vytvoříte zkuste ho nahrát,
do složky tests/MODUL/
kde modul nahraĎte parser, scanner,...

# Testrunner #

Testrunner je napsaný pro node.js, který je nutný ke zprovoznění. Ke stažení na [http://www.nodejs.org](nodejs.org)

Kód by MĚL BÝT multiplatformní, ale bohužel mi v provozu v běžném windowsovském CMD vrací špatné návratové hodnoty. Ale pokuid máte cygwin tak vám to tam poběží (pokud konstalace hvězd dovolí).

aby se příkazy líp psaly, tak jsem zjednodušil název a odstranil příponu.
Z původního 'testrunner.js' na prostý 'test'.
*Válí se tam navolno tak jen připomínám ať si to nespletete za žádný bordel z kompilace, nebo tak :)*

## Definice testu ##

V každém testovacím souboru musí být na prvních dvou řádcích specifický zápis, který se používá ke spuštění testů. Na prvním řádku je očekávaná návratová hodnota, na druhém řádku pak dodatečný popis, který se vypisuje v testrunneru pro přehled co nám vlastně funguje a co hází chyby

Ukázka
```
#!

{exit: 4}
{desc: integer nelze vlozit do boolu}
```

## Před spuštěním ##

Testrunner projíždí testy přes tester.exe (je to správně? nebělo by to být na program.exe?), takže před spuštěním je třeba projekt zkompilovat

## Spuštění ##

Všechny testy
```
#!

node test
```

Specifická složka (možno psát bez složky 'tests')
```
#!

node test tests/semanticke/
node test semanticke
```

Konkrétní test
```
#!

node test "tests/semanticke/assign bool to int.txt"
node test "tests/semanticke/assign bool to int"
node test "semanticke/assign bool to int.txt"
node test "semanticke/assign bool to int"
node test semanticke/assign bool to int.txt
node test semanticke/assign bool to int
```

Pro rychlý přehled o tom které testy prochází a které ne můžete vypnout dumpování stderr danného testu, stačí ještě před zadaním složky/cesty k testu napsat false (...debug mode - false)
```
#!

node test false semanticke/assign bool to int
```