/* ******************************* parser.h ********************************* */
/* Soubor:              parser.h - Hlavickovy soubor pro                      */
/*                                    Syntakticky analyzator (parser)         */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:    -> Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                      Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#ifndef PARSER_H_INCLUDED
#define PARSER_H_INCLUDED
#include "str.h"

#define CONTEXT_LOCAL 0
#define CONTEXT_GLOBAL 1

/**
 * Spuštění lexikální, syntaktické, sémantické analýzy a interpretu
 * @return  int     Navratovy kod z vyctu "tecodes"
*/
int callParser();

/**
* Funkce, která nám vrací z bufferu poslední token - chová se jako fronta
* @param tokenString Ukazatel na token
* @return int Typ tokenu jako celočíselná konstanta
*/
int buffGetToken(string *attr);

/**
* Funkce generuje jedinecne nazvy identifikatoru
* nazev se sklada ze znaku $ nasledovanym cislem
* postupne se tu generuji prirozena cisla a do nazvu promenne se ukladaji
* v reverzovanem poradi - na funkcnost to nema vliv, ale je jednodussi implementace
* @param var Ukazatel na mallocnutý a inilializovaný řetězec
* @return void
*/
void generateVariable(string *var);

/**
* Funkce vlozi novou instrukci do seznamu instrukci
* @param instType Typ instrukce vyjádřený celočíselnou konstantou
* @param instType addr1 Adresa prvního operandu
* @param instType addr2 Adresa druhého operandu
* @param instType addr3 Adresa výsledku
* @return void
*/
void generateInstruction(int instType, void *addr1, void *addr2, void *addr3);

#endif // PARSER_H_INCLUDED
