/* ******************************* parser.h ********************************* */
/* Soubor:              parser.h - Hlavickovy soubor pro                      */
/*                                    Syntakticky analyzator (parser)         */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                      Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#include <stdlib.h>
#include "constants.h"
#include "main.h"
#include "str.h"
#include "parser.h"
#include "expr.h"

/****************** ZACATEK ZASOBNIKOVYCH F-CI ******************/
static int stackInit(precStack *s){
    s->first = malloc(sizeof(struct precData));
    if(s->first == NULL){
        return INTERNAL_ERROR;
    }
    s->first->tokenType = PREC_EOF;
    s->first->ptr = NULL;
    s->top = s->terminalTop = s->first;
    return ST_OK;
}
int stackEmpty(precStack *s){
    if(s->top == s->first){
        return ST_OK;
    }
    return 0;
}
int stackTop(precStack *s, precData *a){
    if(!stackEmpty(s)){
        a->tokenType = s->top->tokenType;
        a->dataType = s->top->dataType;
        a->data = s->top->data;
        a->ptr = s->top->ptr;
        return ST_OK;
    }
    else{
        a->tokenType = s->top->tokenType;
        return ST_ERROR; // dopravdy to neni ERROR, ale potrebuju vedet ze na zasobniku nic neni, abych si pak nevyhodil pocatecni $
    }
}
int stackTermTop(precStack *s){
    return s->terminalTop->tokenType;
}
int stackTopPop(precStack *s, precData *a){
    int copyTop = stackTop(s, a);
    if(copyTop != ST_ERROR){
        if(a->ptr != NULL){
            precData *temp = s->top->ptr;
            free(s->top);
            s->top = temp;
            s->terminalTop = s->top;
        }
        while(stackTermTop(s) == PREC_EXPRESSION){
            s->terminalTop = s->terminalTop->ptr;
        }
        precData te;
    stackTop(s, &te);
        return ST_OK;
    }
    return ST_ERROR;
}
int stackPush(precStack *s, precData a){
    precData *temp = malloc(sizeof(struct precData));
    if(temp == NULL){
        return INTERNAL_ERROR;
    }
    temp->tokenType = a.tokenType;
    temp->dataType = a.dataType;
    temp->data = a.data;
    temp->ptr = s->top;
    s->top = temp;
    if((temp->tokenType >= PREC_EQUAL) && (temp->tokenType <= PREC_NUMBER)){
        s->terminalTop = temp;
    }
    precData te;
    stackTop(s, &te);
    return ST_OK;
}

int stackDestroy(precStack *s){
    while(s->top != NULL){
        precData *temp = s->top;
        s->top = s->top->ptr;
        free(temp);
    }
    return ST_OK;
}
/****************** KONEC ZASOBNIKU ******************/

/****************** ZACATEK PRECEDENCNI TABULKY A PRAVIDEL ******************/

static int precTable[PREC_TABLE_SIZE][PREC_TABLE_SIZE];

void precTableInit(){
    precTable[PREC_EQUAL][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_EQUAL][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_EQUAL][PREC_LESS] = ST_GREATER;
    precTable[PREC_EQUAL][PREC_GREATER] = ST_GREATER;
    precTable[PREC_EQUAL][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_EQUAL][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_EQUAL][PREC_PLUS] = ST_LESS;
    precTable[PREC_EQUAL][PREC_MINUS] = ST_LESS;
    precTable[PREC_EQUAL][PREC_MUL] = ST_LESS;
    precTable[PREC_EQUAL][PREC_DIV] = ST_LESS;
    precTable[PREC_EQUAL][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_EQUAL][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_EQUAL][PREC_BOOLEAN] = ST_LESS;
    precTable[PREC_EQUAL][PREC_STRING] = ST_LESS;
    precTable[PREC_EQUAL][PREC_NUMBER] = ST_LESS;
    precTable[PREC_EQUAL][PREC_EOF] = ST_GREATER;

    precTable[PREC_NOTEQUAL][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_NOTEQUAL][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_NOTEQUAL][PREC_LESS] = ST_GREATER;
    precTable[PREC_NOTEQUAL][PREC_GREATER] = ST_GREATER;
    precTable[PREC_NOTEQUAL][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_NOTEQUAL][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_NOTEQUAL][PREC_PLUS] = ST_LESS;
    precTable[PREC_NOTEQUAL][PREC_MINUS] = ST_LESS;
    precTable[PREC_NOTEQUAL][PREC_MUL] = ST_LESS;
    precTable[PREC_NOTEQUAL][PREC_DIV] = ST_LESS;
    precTable[PREC_NOTEQUAL][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_NOTEQUAL][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_NOTEQUAL][PREC_BOOLEAN] = ST_LESS;
    precTable[PREC_NOTEQUAL][PREC_STRING] = ST_LESS;
    precTable[PREC_NOTEQUAL][PREC_NUMBER] = ST_LESS;
    precTable[PREC_NOTEQUAL][PREC_EOF] = ST_GREATER;

    precTable[PREC_LESS][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_LESS][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_LESS][PREC_LESS] = ST_GREATER;
    precTable[PREC_LESS][PREC_GREATER] = ST_GREATER;
    precTable[PREC_LESS][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_LESS][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_LESS][PREC_PLUS] = ST_LESS;
    precTable[PREC_LESS][PREC_MINUS] = ST_LESS;
    precTable[PREC_LESS][PREC_MUL] = ST_LESS;
    precTable[PREC_LESS][PREC_DIV] = ST_LESS;
    precTable[PREC_LESS][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_LESS][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_LESS][PREC_NUMBER] = ST_LESS;
    precTable[PREC_LESS][PREC_EOF] = ST_GREATER;

    precTable[PREC_GREATER][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_GREATER][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_GREATER][PREC_LESS] = ST_GREATER;
    precTable[PREC_GREATER][PREC_GREATER] = ST_GREATER;
    precTable[PREC_GREATER][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_GREATER][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_GREATER][PREC_PLUS] = ST_LESS;
    precTable[PREC_GREATER][PREC_MINUS] = ST_LESS;
    precTable[PREC_GREATER][PREC_MUL] = ST_LESS;
    precTable[PREC_GREATER][PREC_DIV] = ST_LESS;
    precTable[PREC_GREATER][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_GREATER][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_GREATER][PREC_NUMBER] = ST_LESS;
    precTable[PREC_GREATER][PREC_EOF] = ST_GREATER;

    precTable[PREC_LESS_E][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_LESS_E][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_LESS_E][PREC_LESS] = ST_GREATER;
    precTable[PREC_LESS_E][PREC_GREATER] = ST_GREATER;
    precTable[PREC_LESS_E][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_LESS_E][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_LESS_E][PREC_PLUS] = ST_LESS;
    precTable[PREC_LESS_E][PREC_MINUS] = ST_LESS;
    precTable[PREC_LESS_E][PREC_MUL] = ST_LESS;
    precTable[PREC_LESS_E][PREC_DIV] = ST_LESS;
    precTable[PREC_LESS_E][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_LESS_E][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_LESS_E][PREC_NUMBER] = ST_LESS;
    precTable[PREC_LESS_E][PREC_EOF] = ST_GREATER;

    precTable[PREC_GREATER_E][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_GREATER_E][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_GREATER_E][PREC_LESS] = ST_GREATER;
    precTable[PREC_GREATER_E][PREC_GREATER] = ST_GREATER;
    precTable[PREC_GREATER_E][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_GREATER_E][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_GREATER_E][PREC_PLUS] = ST_LESS;
    precTable[PREC_GREATER_E][PREC_MINUS] = ST_LESS;
    precTable[PREC_GREATER_E][PREC_MUL] = ST_LESS;
    precTable[PREC_GREATER_E][PREC_DIV] = ST_LESS;
    precTable[PREC_GREATER_E][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_GREATER_E][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_GREATER_E][PREC_NUMBER] = ST_LESS;
    precTable[PREC_GREATER_E][PREC_EOF] = ST_GREATER;

    precTable[PREC_PLUS][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_PLUS][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_PLUS][PREC_LESS] = ST_GREATER;
    precTable[PREC_PLUS][PREC_GREATER] = ST_GREATER;
    precTable[PREC_PLUS][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_PLUS][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_PLUS][PREC_PLUS] = ST_GREATER;
    precTable[PREC_PLUS][PREC_MINUS] = ST_GREATER;
    precTable[PREC_PLUS][PREC_MUL] = ST_LESS;
    precTable[PREC_PLUS][PREC_DIV] = ST_LESS;
    precTable[PREC_PLUS][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_PLUS][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_PLUS][PREC_STRING] = ST_LESS;
    precTable[PREC_PLUS][PREC_NUMBER] = ST_LESS;
    precTable[PREC_PLUS][PREC_EOF] = ST_GREATER;

    precTable[PREC_MINUS][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_MINUS][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_MINUS][PREC_LESS] = ST_GREATER;
    precTable[PREC_MINUS][PREC_GREATER] = ST_GREATER;
    precTable[PREC_MINUS][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_MINUS][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_MINUS][PREC_PLUS] = ST_GREATER;
    precTable[PREC_MINUS][PREC_MINUS] = ST_GREATER;
    precTable[PREC_MINUS][PREC_MUL] = ST_LESS;
    precTable[PREC_MINUS][PREC_DIV] = ST_LESS;
    precTable[PREC_MINUS][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_MINUS][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_MINUS][PREC_NUMBER] = ST_LESS;
    precTable[PREC_MINUS][PREC_EOF] = ST_GREATER;

    precTable[PREC_MUL][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_MUL][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_MUL][PREC_LESS] = ST_GREATER;
    precTable[PREC_MUL][PREC_GREATER] = ST_GREATER;
    precTable[PREC_MUL][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_MUL][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_MUL][PREC_PLUS] = ST_GREATER;
    precTable[PREC_MUL][PREC_MINUS] = ST_GREATER;
    precTable[PREC_MUL][PREC_MUL] = ST_GREATER;
    precTable[PREC_MUL][PREC_DIV] = ST_GREATER;
    precTable[PREC_MUL][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_MUL][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_MUL][PREC_NUMBER] = ST_LESS;
    precTable[PREC_MUL][PREC_EOF] = ST_GREATER;

    precTable[PREC_DIV][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_DIV][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_DIV][PREC_LESS] = ST_GREATER;
    precTable[PREC_DIV][PREC_GREATER] = ST_GREATER;
    precTable[PREC_DIV][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_DIV][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_DIV][PREC_PLUS] = ST_GREATER;
    precTable[PREC_DIV][PREC_MINUS] = ST_GREATER;
    precTable[PREC_DIV][PREC_MUL] = ST_GREATER;
    precTable[PREC_DIV][PREC_DIV] = ST_GREATER;
    precTable[PREC_DIV][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_DIV][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_DIV][PREC_NUMBER] = ST_LESS;
    precTable[PREC_DIV][PREC_EOF] = ST_GREATER;

    precTable[PREC_BRACKET_L][PREC_EQUAL] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_NOTEQUAL] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_LESS] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_GREATER] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_LESS_E] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_GREATER_E] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_PLUS] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_MINUS] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_MUL] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_DIV] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_BRACKET_R] = ST_EQUAL;
    precTable[PREC_BRACKET_L][PREC_BOOLEAN] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_STRING] = ST_LESS;
    precTable[PREC_BRACKET_L][PREC_NUMBER] = ST_LESS;

    precTable[PREC_BRACKET_R][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_LESS] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_GREATER] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_PLUS] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_MINUS] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_MUL] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_DIV] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_BRACKET_R][PREC_EOF] = ST_GREATER;

    precTable[PREC_BOOLEAN][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_BOOLEAN][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_BOOLEAN][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_BOOLEAN][PREC_EOF] = ST_GREATER;

    precTable[PREC_STRING][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_STRING][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_STRING][PREC_PLUS] = ST_GREATER;
    precTable[PREC_STRING][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_STRING][PREC_EOF] = ST_GREATER;

    precTable[PREC_NUMBER][PREC_EQUAL] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_NOTEQUAL] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_LESS] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_GREATER] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_LESS_E] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_GREATER_E] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_PLUS] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_MINUS] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_MUL] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_DIV] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_BRACKET_R] = ST_GREATER;
    precTable[PREC_NUMBER][PREC_EOF] = ST_GREATER;

    precTable[PREC_EOF][PREC_EQUAL] = ST_LESS;
    precTable[PREC_EOF][PREC_NOTEQUAL] = ST_LESS;
    precTable[PREC_EOF][PREC_LESS] = ST_LESS;
    precTable[PREC_EOF][PREC_GREATER] = ST_LESS;
    precTable[PREC_EOF][PREC_LESS_E] = ST_LESS;
    precTable[PREC_EOF][PREC_GREATER_E] = ST_LESS;
    precTable[PREC_EOF][PREC_PLUS] = ST_LESS;
    precTable[PREC_EOF][PREC_MINUS] = ST_LESS;
    precTable[PREC_EOF][PREC_MUL] = ST_LESS;
    precTable[PREC_EOF][PREC_DIV] = ST_LESS;
    precTable[PREC_EOF][PREC_BRACKET_L] = ST_LESS;
    precTable[PREC_EOF][PREC_BOOLEAN] = ST_LESS;
    precTable[PREC_EOF][PREC_STRING] = ST_LESS;
    precTable[PREC_EOF][PREC_NUMBER] = ST_LESS;
    precTable[PREC_EOF][PREC_EOF] = ST_CHECK;

}

static int precRules[PREC_RULES][3] = { //semanticka pravidla, zpracovany v obracenem poradi
    {PREC_NUMBER, 0, 0},//0
    {PREC_STRING, 0, 0},//1
    {PREC_BOOLEAN, 0, 0},//2
    {PREC_EXPRESSION, PREC_PLUS, PREC_EXPRESSION},//3
    {PREC_EXPRESSION, PREC_MINUS, PREC_EXPRESSION},//4
    {PREC_EXPRESSION, PREC_MUL, PREC_EXPRESSION},//5
    {PREC_EXPRESSION, PREC_DIV, PREC_EXPRESSION},//6
    {PREC_BRACKET_R, PREC_EXPRESSION, PREC_BRACKET_L},//7
    {PREC_EXPRESSION, PREC_GREATER_E, PREC_EXPRESSION},//8
    {PREC_EXPRESSION, PREC_LESS_E, PREC_EXPRESSION},//9
    {PREC_EXPRESSION, PREC_NOTEQUAL, PREC_EXPRESSION},//10
    {PREC_EXPRESSION, PREC_EQUAL, PREC_EXPRESSION},//11
    {PREC_EXPRESSION, PREC_GREATER, PREC_EXPRESSION},//12
    {PREC_EXPRESSION, PREC_LESS, PREC_EXPRESSION}//13
};

/****************** KONEC TABULKY A PRAVIDEL ******************/
/****************** PRECEDENCNI ANALYZA *******************/
static int tokenType;
static string token;
static table *tableG;
static table *tableL;
static precData tokenToPush;
static int context;
static int error;
static int lastToken;
static Data *firstInstruction = NULL;
static int breakPoint;

int generateNewItem(int ctx){ //vygeneruje novou polozku lok. nebo glob. TS pro mezivysledky a literaly
    string name;
    tableItem *item;
    tableItem *test;
    if((error = strInit(&name)) == STR_ERROR){
        error = INTERNAL_ERROR;
        return ST_ERROR;
    }

    generateVariable(&name);

    if(ctx == CONTEXT_LOCAL){
        item = saveItem(tableL, &name, VARIABLE, &error);
        if(error == INTERNAL_ERROR){
            return ST_ERROR;
        }
    }
    else{
        item = saveItem(tableG, &name, VARIABLE, &error);
        if(error == INTERNAL_ERROR){
            return ST_ERROR;
        }
    } 
    if(tokenToPush.dataType == DT_STRING){
        strInit(&(item->data->value.strVal));
    }  
    tokenToPush.data = item->data;
    tokenToPush.data->dataType = tokenToPush.dataType;
}
int createInstruction(int ctx, int instType, void *op1, void *op2){
    generateNewItem(ctx);
    generateInstruction(instType, op1, op2, (void *)tokenToPush.data);
}

int encode(int tokenType){ // nastavime celou tabulku na CHYBU tim zarucime to, ze vsechny ostatni polozky tabulky, ktere pozdeji nenastavime budou znacit CHYBU.
    int encodeTable[66];
    int i = 0;
    for(i; i < 66; i++){
        encodeTable[i] = ST_ERROR;
    }
    encodeTable[LIT_INTEGER] = PREC_EXPRESSION;
    encodeTable[LIT_REAL] = PREC_EXPRESSION;
    encodeTable[LIT_STRING] = PREC_EXPRESSION;
    encodeTable[KW_TRUE] = PREC_EXPRESSION;
    encodeTable[KW_FALSE] = PREC_EXPRESSION;
    encodeTable[OP_EQUAL] = PREC_EQUAL;
    encodeTable[OP_LESS_MORE] = PREC_NOTEQUAL;
    encodeTable[OP_LESS] = PREC_LESS;
    encodeTable[OP_MORE] = PREC_GREATER;
    encodeTable[OP_LESS_EQUAL] = PREC_LESS_E;
    encodeTable[OP_MORE_EQUAL] = PREC_GREATER_E;
    encodeTable[OP_ADD] = PREC_PLUS;
    encodeTable[OP_SUB] = PREC_MINUS;
    encodeTable[OP_MUL] = PREC_MUL;
    encodeTable[OP_DIV] = PREC_DIV;
    encodeTable[BRACKET_L] = PREC_BRACKET_L;
    encodeTable[BRACKET_R] = PREC_BRACKET_R;
    encodeTable[END] = PREC_EOF;
    encodeTable[KW_END] = PREC_EOF;
    encodeTable[KW_THEN] = PREC_EOF;
    encodeTable[KW_DO] = PREC_EOF;
    encodeTable[SEMICOLON] = PREC_EOF;

    if(tokenType == ID){
        error = NOT_FOUND;
        tableItem *item;

        // pokusim se najit identifikator v Globalni tabulce
        if(context == CONTEXT_LOCAL){
            item = searchItem(tableL, &token, VARIABLE, &error);
        }
        if(error == NOT_FOUND){
            item = searchItem(tableG, &token, VARIABLE, &error);
        }
        if(error == NOT_FOUND){
            error = ESEM_D;
            return ST_ERROR;
        }

        // promennou sme nasli tak ji konecne muzeme zacit zpracovavat.
        tokenToPush.dataType = item->data->dataType; // potrebujeme vlastne jen datovy typ.
        tokenToPush.data = item->data;
        return PREC_EXPRESSION; // misto identifikatoru uz ale vratime expression, aby se nam tan uz nemotal.
    }
    else if(tokenType == KW_FALSE || tokenType == KW_TRUE){  // vytvori to novou polozku v globalni tabulce symbolu
        generateNewItem(CONTEXT_GLOBAL);

        tokenToPush.data->identifierType = VARIABLE; // ulozi to postupne vsechny dulezite informace o promenne
        tokenToPush.data->dataType = DT_BOOLEAN;
        tokenToPush.data->initialized = true;
        tokenToPush.dataType = DT_BOOLEAN;

        if(tokenType == KW_FALSE){ // podle toho jestli sme dostali TRUE nebo FALSE tak to nastavi bool hodnotu
            tokenToPush.data->value.boolVal = false;
        }
        else{
            tokenToPush.data->value.boolVal = true;
        }
    }
    else if(tokenType == LIT_INTEGER){ // to same jako predesla funkce jen pro integer
        //printf("UKLADAM CISLO\n");
        fflush( stdout );
        generateNewItem(CONTEXT_GLOBAL);

        tokenToPush.data->identifierType = VARIABLE;
        tokenToPush.data->dataType = DT_INTEGER;
        tokenToPush.data->initialized = true;
        tokenToPush.dataType = DT_INTEGER;

        tokenToPush.data->value.intVal = atoi(token.str);
    }
    else if(tokenType == LIT_REAL){ // to same jako predesla funkce jen pro real
        generateNewItem(CONTEXT_GLOBAL);

        tokenToPush.data->identifierType = VARIABLE;
        tokenToPush.data->dataType = DT_REAL;
        tokenToPush.data->initialized = true;
        tokenToPush.dataType = DT_REAL;

        tokenToPush.data->value.realVal = atof(token.str);
    }
    else if(tokenType == LIT_STRING){   // to same jako predesla funkce jen pro string
        generateNewItem(CONTEXT_GLOBAL);

        tokenToPush.data->identifierType = VARIABLE;
        tokenToPush.data->dataType = DT_STRING;
        tokenToPush.data->initialized = true;
        tokenToPush.dataType = DT_STRING;

        strInit(&(tokenToPush.data->value.strVal));
        strCpy(&(tokenToPush.data->value.strVal), &token);
    }
    else if(tokenType == KW_DO || tokenType == KW_THEN){
        breakPoint = tokenType;
    }
    error = EPARS; // nastaví se pro případ že se nepřekódoval token. pokud vse probehlo tak jak ma tak to nema zadny smysl.
    return encodeTable[tokenType];

}

/**
* Funkce si necha od Parseru poslat token a prekoduje jeho typ pro vyuziti v Prec.Analyze
*/
void getEncodedToken(){ // pozada parser o token a ten prekonvertuje pro pouziti v expr
    lastToken = buffGetToken(&token);
    if(lastToken < 0){
        error = lastToken;
        return ST_ERROR;
    }
    tokenToPush.tokenType = tokenType = encode(lastToken);
}

/**
* Kontrola zda jsou obe pole identicka
*/
int checkArrays(int *a1,int a2[3]){ // zkontroluje dve pole pravidel, jestli jsou stejne
    int i = 0;
    for(i; i < 3; i++){
        if(a1[i] != a2[i]){
            return ST_ERROR;
        }
    }
    return ST_OK;
}

/**
* Funkce hleda, zda nactene tokeny odpovidaji nekteremu syntaktickemu pravidlu
* @params: precData preparedRule[] - pole s nactenyma tokenama ze zasobniku
* @return: int - ST_OK nebo ST_ERROR
*/
int checkTheRule(precData preparedRule[3]){ // snazi se najit jestli to co sem vytahnul ze zasobniku odpovida nejakemu pravidlu
    int i = 0;
    int myRule[3] = { preparedRule[0].tokenType, preparedRule[1].tokenType, preparedRule[2].tokenType };
    //printf("Pravidlo: [%d, %d, %d]\n", myRule[0], myRule[1], myRule[2]);
    for(i; i <= PREC_RULES; i++){
        if(checkArrays(&precRules[i], myRule) == ST_OK){ // pokud to naslo odpovidajici pravidlo
            return i; // tak vrati jeho cislo
        }
    }
    error = EPARS;
    return ST_ERROR;
}

/**
* Funkce, ktera kontroluje semantike pravidla pro vsechny pravidla (+, -, *, /, ...)
* @params:  int rule - cislo syntaktickeho pravidla
*           precData preparedTokens[3] - pole s tokenama ze zasobniku
* @return:  int - ST_OK nebo ST_ERROR
*/
int semanticAnalyze(int rule, precData preparedTokens[3]){ // zpracovani semantickych pravidel
    //printf("pravidlo %d\n", rule);
    int tokens[3] = { preparedTokens[0].tokenType, preparedTokens[1].tokenType, preparedTokens[2].tokenType };
    int dataTypes[3] = { preparedTokens[0].dataType, preparedTokens[1].dataType, preparedTokens[2].dataType };
    switch(rule){

        // pravidlo E -> number
        case 0:
            tokenToPush.dataType = DT_INTEGER;
        break;

        // pravidlo E -> string
        case 1:
            tokenToPush.dataType = DT_STRING;
        break;

        // pravidlo E -> boolean
        case 2:
            tokenToPush.dataType = DT_BOOLEAN;
        break;

        // pravidlo E -> E + E
        case 3:
            if((dataTypes[0] != DT_BOOLEAN) && (dataTypes[2] != DT_BOOLEAN)){
                if(dataTypes[0] == dataTypes[2]){
                    tokenToPush.dataType = dataTypes[0];
                    //printf("Tady este su\n");
                    if(dataTypes[0] == DT_STRING){
                        createInstruction(context, I_STR_CONCAT, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                        return ST_OK;
                    }
                    else{
                        createInstruction(context, I_ADD, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                        return ST_OK;
                    }
                }
                else if(((dataTypes[0] == DT_REAL) &&(dataTypes[2] == DT_INTEGER)) || ((dataTypes[0] == DT_INTEGER) && (dataTypes[2] == DT_REAL))){
                    tokenToPush.dataType = DT_REAL;
                    //printf("NE ja sem este tu");
                    createInstruction(context, I_ADD, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                    //printf("VYTVORIL sem instrukci :)\n");
                    return ST_OK;
                }
                else{
                    error = ESEM_M;
                    return ST_ERROR;
                }
            }
            else{
                error = ESEM_M;
                return ST_ERROR;
            }
        break;

        // pravidlo E -> E - E
        case 4:
            if(((dataTypes[0] != DT_BOOLEAN) && (dataTypes[2] != DT_BOOLEAN)) && ((dataTypes[0] != DT_STRING) && (dataTypes[2] != DT_STRING))){
                if(dataTypes[0] == dataTypes[2]){
                    tokenToPush.dataType = dataTypes[0];
                    createInstruction(context, I_SUB, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                    return ST_OK;
                }
                else if(((dataTypes[0] == DT_REAL) &&(dataTypes[2] == DT_INTEGER)) || ((dataTypes[0] == DT_INTEGER) && (dataTypes[2] == DT_REAL))){
                    tokenToPush.dataType = DT_REAL;
                    createInstruction(context, I_SUB, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                    return ST_OK;
                }
                else{
                    error = ESEM_M;
                    return ST_ERROR;
                }
            }
            else{
                error = ESEM_M;
                return ST_ERROR;
            }
        break;

        // pravido E -> E * E
        case 5:
            if(((dataTypes[0] != DT_BOOLEAN) && (dataTypes[2] != DT_BOOLEAN)) && ((dataTypes[0] != DT_STRING) && (dataTypes[2] != DT_STRING))){
                if(dataTypes[0] == dataTypes[2]){
                    tokenToPush.dataType = dataTypes[0];
                    createInstruction(context, I_MUL, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                    return ST_OK;
                }
                else if(((dataTypes[0] == DT_REAL) &&(dataTypes[2] == DT_INTEGER)) || ((dataTypes[0] == DT_INTEGER) && (dataTypes[2] == DT_REAL))){
                    tokenToPush.dataType = DT_REAL;
                    createInstruction(context, I_MUL, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                    return ST_OK;
                }
                else{
                    error = ESEM_M;
                    return ST_ERROR;
                }
            }
            else{
                error = ESEM_M;
                return ST_ERROR;
            }
        break;

        // pravidlo E -> E / E
        case 6:
            if(((dataTypes[0] != DT_BOOLEAN) && (dataTypes[2] != DT_BOOLEAN)) && ((dataTypes[0] != DT_STRING) && (dataTypes[2] != DT_STRING))){
                if(dataTypes[0] == dataTypes[2]){
                    tokenToPush.dataType = DT_REAL;
                    createInstruction(context, I_DIV, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                    return ST_OK;
                }
                else{
                    error = ESEM_M;
                    return ST_ERROR;
                }
            }
            else{
                error = ESEM_M;
                return ST_ERROR;
            }
        break;

        // pravidlo E -> (E)
        case 7:
            tokenToPush.dataType = dataTypes[1];
        break;

        // pravidlo E -> E >= E
        case 8:
            if(((dataTypes[0] == DT_INTEGER) && (dataTypes[2] == DT_INTEGER)) || ((dataTypes[0] == DT_REAL) && (dataTypes[2] == DT_REAL))){
                tokenToPush.dataType = DT_BOOLEAN;
                createInstruction(context, I_MORE_EQUAL, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                return ST_OK;
            }
            else{
                error = ESEM_M;
                return ST_ERROR;
            }
        break;

        // pravidlo E -> E <= E
        case 9:
            if(((dataTypes[0] == DT_INTEGER) && (dataTypes[2] == DT_INTEGER)) || ((dataTypes[0] == DT_REAL) && (dataTypes[2] == DT_REAL))){
                tokenToPush.dataType = DT_BOOLEAN;
                createInstruction(context, I_LESS_EQUAL, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                return ST_OK;
            }
            else{
                error = ESEM_M;
                return ST_ERROR;
            }
        break;

        // pravidlo E -> E <> E
        case 10:
            if(dataTypes[0] == dataTypes[2]){
                tokenToPush.dataType = DT_BOOLEAN;
                createInstruction(context, I_LESS_MORE, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                return ST_OK;
            }
            else{
                error = ESEM_M;
                return ST_ERROR;
            }
        break;

        // pravidlo E -> E = E
        case 11:
            if(dataTypes[0] == dataTypes[2]){
                tokenToPush.dataType = DT_BOOLEAN;
                createInstruction(context, I_EQUAL, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                return ST_OK;
            }
            else{
                error = ESEM_M;
                return ST_ERROR;
            }
        break;

        // pravidlo E -> E > E
        case 12:
            if(((dataTypes[0] == DT_INTEGER) && (dataTypes[2] == DT_INTEGER)) || ((dataTypes[0] == DT_REAL) && (dataTypes[2] == DT_REAL))){
                tokenToPush.dataType = DT_BOOLEAN;
                createInstruction(context, I_MORE, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                return ST_OK;
            }
            else{
                error = ESEM_M;
                return ST_ERROR;
            }
        break;

        // pravidlo E -> E < E
        case 13:
            if(((dataTypes[0] == DT_INTEGER) && (dataTypes[2] == DT_INTEGER)) || ((dataTypes[0] == DT_REAL) && (dataTypes[2] == DT_REAL))){
                tokenToPush.dataType = DT_BOOLEAN;
                createInstruction(context, I_LESS, (void *)(preparedTokens[2].data), (void *)(preparedTokens[0].data));
                return ST_OK;
            }
            else{
                error = ESEM_M;
                return ST_ERROR;
            }
        break;
    }
}

/**
* Funkce starajici se o konvertovani tokenu na vyraz a volani syntakticke a semanticke kontroly
* @params: precStack *stack - zasobnik se vsema potrebnyma tokenama
* @return: int - ST_OK nebo ST_ERROR
*/
int convertToExpression(precStack *stack){ // zpracovani vyrazu pomoci pravidel
    precData stackedToken; // prave nacteny token ze zasobniku
    precData preparedTokens[3] = {stackedToken, stackedToken, stackedToken}; // pole pro vytvoreni pravidla ktere dostaneme ze zasobniku
    int i = 0; //pocitadlo
    int rule;
    error = stackTopPop(stack, &stackedToken);
    do{
        if(error == ST_ERROR || i >= 3){ // pokud mame token, nebo pokud uz nahodou vime ze mame vic tokenu nez ze kterych muzeme udelat pravidlo
            error = EPARS;

            return ST_ERROR;
        }
        preparedTokens[i].tokenType = stackedToken.tokenType;
        preparedTokens[i].dataType = stackedToken.dataType;
        preparedTokens[i++].data = stackedToken.data;
        error = stackTopPop(stack, &stackedToken);
    }while(stackedToken.tokenType != ST_LESS); // cyklus bezi dokud nenarazi na "<"
    rule = checkTheRule(preparedTokens); //preloz mi tokeny pomoci nejakeho pravidla na expression
    if(rule == ST_ERROR){ // pokud neslo prelozit CHYBA
        error = EPARS;
        return ST_ERROR;
    }
    tokenToPush.tokenType = PREC_EXPRESSION;
    if(semanticAnalyze(rule, preparedTokens) == ST_ERROR){ // zkontroluj jestli koresponduji tadove typy a nastav vysledny datovy typ
        return ST_ERROR;
    }
    stackPush(stack, tokenToPush);
    return ST_OK;
}

/**
* Hlavní funkce precedencni analyzy, ktera vyhodnocuje vyrazy
*
* @params:  table *localtable - lokalni taulka symbolu
*           table *globalTable- globalnni tabulka symbolu
*           int ctx - Context muze nabyvat hodnot CONTEXT_LOCAL nebo CONTEXT_GLOBAL
*           int expectedDataType - oznacuje jakoho datoveho typu ma byt vysledek
*           int *lastTok - parametr pro vrace posledniho tokenu, ktery sem nacetl
*           Data *variableData = parametr pro vraceni vysledne hodnoty
*           tListItem *firstGenerInstruc - nepouzivany parametr pro vraceni prvni vygenerovane instrukce
* @return:  int - vraci ST_OK nebo cislo chyby
*           int *lastTok
*           Data *variableData
*/
int precedenceAnalyze(table *localTable, table *globalTable, int ctx, int expectedDataType, int *lastTok, Data *variableData, tListItem* firstGenerInstruc){

    if (strInit(&token) == STR_ERROR){ // inicializujeme proměnnou reprezentujcí token
      return INTERNAL_ERROR;
    }
    tableG = globalTable;
    tableL = localTable;
    context = ctx;
    int stackedToken;
    int action;
    int rule;

    precTableInit();
    precStack stack;
    stackInit(&stack);

    getEncodedToken(); //nacteni prvniho tokenu

    if(tokenType == ST_ERROR){ // a jeho kontrola
        strFree(&token);
        stackDestroy(&stack);
        return error;
    }

    stackedToken = stackTermTop(&stack); // nacte posledni terminal na zasobniku


    while((tokenType != PREC_EOF) || (stackedToken != PREC_EOF)){
        if(tokenType == PREC_EXPRESSION){ // pokud nam prisel identifikator tak mame v token type vlastne exxpresin a to nezvladne tabulka.
            action = ST_EQUAL; // tak nastavime at se proste jen prihodi na zasobnik.
        }
        else{
            action = precTable[stackedToken][tokenType]; // vyhleda v precedencni tabulce jaka akce se ma provest
        }
        switch(action){
            case ST_EQUAL: // pouze pushne token na zasobnik a pozada o token dalsi
                tokenToPush.tokenType = tokenType;
                stackPush(&stack, tokenToPush);
                if(tokenType != PREC_EOF){ 
                    getEncodedToken();
                    if(tokenType == ST_ERROR){
                        strFree(&token);
                        stackDestroy(&stack);
                        return error;
                    }
                }
            break;
            case ST_LESS:
                stackTop(&stack, &tokenToPush); // podivame se co mame na vrchu zasobnika
                    if (tokenToPush.tokenType == PREC_EXPRESSION){ //pokud to neni EOF tedy na zasobniku uz neco je
                        stackTopPop(&stack, &tokenToPush); // vytahneme si to
                        precData temp;
                        temp.tokenType = ST_LESS;
                        stackPush(&stack, temp); // misto toho vytazeneho tam vlozime <
                        stackPush(&stack, tokenToPush); // vlzime tam zpet vytazeny token
                        tokenToPush.tokenType = tokenType; // pripravime si token co mame na vstupu
                        stackPush(&stack, tokenToPush); // a vlozime ho do zasobniku
                    }
                    else{ // pokud je ale na vrchu zasobniku EOF tj. v zasobniku este nic neni
                        tokenToPush.tokenType = ST_LESS;
                        stackPush(&stack, tokenToPush); // vlozime na zasobnik <
                        tokenToPush.tokenType = tokenType; // pripravime si token ze vstupu
                        stackPush(&stack, tokenToPush); // a vlozime ho do zasobniku
                    }
                
                if(tokenType != PREC_EOF){
                    getEncodedToken();
                    if(tokenType == ST_ERROR){
                        strFree(&token);
                        stackDestroy(&stack);
                        return error;
                    }
                }
            break;

            case ST_GREATER: // srdce analyzi, ktere kona redukci tokenu na zasobniku a kontrolu vsech veci
                if((rule = convertToExpression(&stack)) == ST_ERROR){
                    strFree(&token);
                    stackDestroy(&stack);
                    return error;
                }
            break;

            case ST_CHECK: //pravdepodobne nepotrebny stav ale pro jistotu :)
                error = stackTop(&stack, &tokenToPush);
                if(tokenToPush.tokenType == PREC_EXPRESSION){
                    strFree(&token);
                    if(tokenToPush.dataType == expectedDataType){
                        *lastTok = lastToken;
                        if(variableData != NULL){
                            generateInstruction(I_ASSIGN, (void *)tokenToPush.data, NULL, (void *)variableData);
                        }
                        else{
                            if(breakPoint == KW_DO){
                                generateInstruction(I_WHILE, (void*)tokenToPush.data, (void*)NULL, (void*)NULL);
                            }
                            else if(breakPoint == KW_THEN){
                                generateInstruction(I_IF, (void*)tokenToPush.data, (void*)NULL, (void*)NULL);
                            }
                        }
                        stackDestroy(&stack);
                        return EOK;
                    }
                    else{
                        stackDestroy(&stack);
                        return ESEM_M;
                    }
                }
                else{
                    strFree(&token);
                    stackDestroy(&stack);
                    return EPARS;
                }
            break;

            default:
                strFree(&token);
                stackDestroy(&stack);
                return EPARS;
            break;
        }

        stackedToken = stackTermTop(&stack);
    }
    error = stackTop(&stack, &tokenToPush);
    if(tokenToPush.tokenType == PREC_EXPRESSION){
        strFree(&token);
        if(tokenToPush.dataType == expectedDataType){
            *lastTok = lastToken;
            if(variableData != NULL){
                generateInstruction(I_ASSIGN, (void *)tokenToPush.data, NULL, (void *)variableData);
            }
            else{
                if(breakPoint == KW_DO){
                    generateInstruction(I_WHILE, (void*)tokenToPush.data, (void*)NULL, (void*)NULL);
                }
                else if(breakPoint == KW_THEN){
                    generateInstruction(I_IF, (void*)tokenToPush.data, (void*)NULL, (void*)NULL);
                }
                else{
                    stackDestroy(&stack);
                    return INTERNAL_ERROR;
                }
            }
            stackDestroy(&stack);
            return EOK;
        }
        else{
            stackDestroy(&stack);
            return ESEM_M;
        }
    }
    strFree(&token);
    stackDestroy(&stack);
    return EPARS;
}
