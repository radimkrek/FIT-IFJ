/* ********************************* ial.h ********************************** */
/* Soubor:              ial.h - Hlavickovy soubor pro                         */
/*                                tabulku, radici a vyhledavaci algoritmus    */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:    -> Jan Herec             xherec00                        */
/*                   -> Radim Křek            xkrekr00                        */
/*                   -> Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                   -> Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#ifndef IAL_H_INCLUDED
#define IAL_H_INCLUDED

#include "constants.h"
#include "str.h"

#define TABLESIZE 101

/* ****************************** Datové struktury ****************************** */

// datová struktura union pro uložení hodnoty proměnné, která může být různých datových typů
union ValueOfVar{
  bool boolVal;
  int intVal;
  double realVal;
  string strVal;
} ValueOfVar;

// struktura pro uložení názvu parametru, názvy parametrů jsou implementovány pomocí zřetězeného seznamu
typedef struct ParamsNames{
  int index;                // pořadí parametru, začíná se prvním parametrem 1
  string *paramName;        // název parametru
  struct ParamsNames* next; // ukazatel na dalsi jméno parametru
} ParamsNames;

typedef struct tableItem{
  int key;                // klic položky v TS
  struct Data* data;      // ukazatel na datovou složku položky
  struct tableItem* next; // ukazatel na dalsi polozku v zřetězeném seznamu
} tableItem;

typedef tableItem* table[TABLESIZE]; // Tabulka symbolů

#include "instructions.h"

// struktura pro reprezentaci datové složky položky v TS
typedef struct Data{
  union ValueOfVar value;               // hodnota proměnné
  string *name;                         // jmeno tokenu ze scaneru
  string *paramsTypes;                  // typ parametrů ulozenych jako retezec napr.: icb
  ParamsNames *paramsNames;             // zřetězený seznam názvů parametrů
  int dataType;                         // datový typ identifikátoru
  int identifierType;                   // typ symbolu. Funkce, Promenna
  bool declared;                        // příznak jestli je proměnná, nebo funkce deklarována
  bool defined;                         // příznak jestli je funkce definována
  bool initialized;                     // příznak jestli byla daná proměnná inicializovaná
  table* localTS;                       // odkaz na lokální tabulku symbolů pro danou funkci
  struct listItem* firstInstructInFunc; // ukazatel na první instrukci dané funkce
  struct listItem* returnInstruct;      // ukazatel na navratovou instrukci po navrácení se z těla dané funkce
} Data;

/* ****************************** Prototypy Funkcí ****************************** */

/**
* Inicializuje Tabulku Symbolu
* @params: table*
* @return: NONE
*/
int newTable(table* t);

/**
* Ulozi symbol do tabulky a vrati ukazatel na daný uložený symbol.
* @param t Ukazatel na TS, do které budeme symbol vkládat
* @param s Ukazatel na řetězec, který reprezentuje název symbolu
* @param identifierType Typ identifikátoru: VARIABLE nebo FUNKCE
* @param Error Chybový kód
* @return tableItem* Vrací ukazatel na uložený symbol
*/
tableItem* saveItem(table* t, string *s, int type, int *Error);

/**
* Vyhledá symbol v TS a vrátí jej,
* @param t Ukazatel na TS, do které budeme symbol vkládat
* @param s Ukazatel na řetězec, který reprezentuje název symbolu
* @param identifierType Typ identifikátoru: VARIABLE nebo FUNKCE
* @param Error Chybový kód - příznak úspěšného nalezení, nebo neúspěšného nalezení symbolu
* @return tableItem* Vrací ukazatel na nalezený symbol
*/
tableItem* searchItem(table *t, string *s, int type, int *Error);

/**
* Odstraní danou položku z TS
* @param i Ukazatel na TS ve které se položka nachází
* @param i Ukazatel na položku v TS
* @return void
*/
void deleteItem(table *t, tableItem *i);

/**
* Odstraní danou TS
* @param i Ukazatel na TS, kterou požadujeme smazat
* @return void
*/
void deleteTable(table *t);

/**
* Zkopíruje tabulku symbolů a vrátí ukazatel do paměti na tuto kopii
* @param tablePtr Ukazatel na tabulku, jejíž kopii budeme dělat
* @param Error Chybový kód - příznak úspěšně vytvořené kopie TS, nebo úspěšně vytvořené kopie
* @return void
*/
table* copyTable(table* tablePtr, int *Error);

/**
* Seradi zadany retezec "s"
*  -pouzita metoda "ShellSort"
*  -radi podle ordinalni hodnoty znaku od nejmensiho po nejvetsi
* @param  *s  Ukazatel na razeny retezec
*/
void strSort(char *s);

#define STR_SIZE  256 // pocet pouzitelnych znaku v retezcich 0..255

/**
* Vyhleda prvni vyskyt podretezce "p" v retezci "t"
*  -pouzita metoda "Boyer-Moor algorithm"
*  -indexovano od 1 (prvni znak retezce ma pozici 1)
* @param   *t   Ukazatel na retezec (text)
* @param   *p   Ukazatel na hledany podretezec (pattern)
* @return  int  Pozice prvniho vyskytu podretezce
* @return  1    Pokud (p == NULL)
* @return  0    Pokud podretezec nebyl nalezen
*/
int strFind(char *t, char *p);

/**
* Vrati podretezec zadaneho retezce "s"
* @param   *s        Ukazatel na retezec
* @param   position  Zacatek pozadovaneho podretezce
* @param   length    Delka pozadovaneho podretezce
* @return  char*     Serazeny retezec
*/
char *strCopy(char *str, int position, int length);

#endif//IAL_H_INCLUDED
