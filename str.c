/* ********************************* str.c ********************************** */
/* Soubor:              str.c - Prace s retezci                               */
/* Kodovani:            UTF-8                                                 */
/* Datum:               rijen.2014                                            */
/* Predmet:             Formalni jazyky a prekladace (IFJ)                    */
/* Projekt:             Implementace interpretu jazyka IFJ14                  */
/* Varianta zadani:                                                           */
/* Autori, login:       Jan Herec             xherec00                        */
/*                      Radim Křek            xkrekr00                        */
/*                   -> Pavel Juhaňák         xjuhan01                        */
/*                      Patrik Unzeitig       xunzei02                        */
/*                      Michal Kovařík        xkovar70                        */
/* ************************************************************************** */

#include <string.h>
#include <malloc.h>
#include "str.h"

#define STR_BLOCK   8  // velikost alokacniho bloku

/**
* Inicializuje strukturu a alokuje misto pro retezec
* @param  *s  Ukazatel na strukturu
* @return  0  Pri uspechu
* @return  1  Pri neuspesne alokaci
*/
int  strInit(string *s){
  if((s->str = (char*) malloc(STR_BLOCK)) == NULL)
    return STR_ERROR;
  s->str[0] = '\0';
  s->length = 0;
  s->allocSize = STR_BLOCK;
  return STR_SUCCESS;
}

/**
* Uvolni pamet pouzivanou retezcem
* @param  *s  Ukazatel na strukturu
*/
void strFree(string *s){
  free(s->str);
  s->length = 0;
  s->allocSize = 0;
}

/**
* Vymaze obsah struktury
* @param  *s  Ukazatel na strukturu
*/
void strClr(string *s){
  s->str[0] = '\0';
  s->length = 0;
}

/**
* Prida znak na konec retezce
* @param  *s  Ukazatel na strukturu
* @param   c  Pridavany znak
* @return  0  Pri uspechu
* @return  1  Pri neuspesne realokaci
*/
int  strCat(string *s, char c){
  if(s->length + 1 >= s->allocSize){ // nedostatek alokovane pameti
    if((s->str = (char*) realloc(s->str, s->allocSize + STR_BLOCK)) == NULL)
      return STR_ERROR;
    s->allocSize = s->allocSize + STR_BLOCK;
  }
  s->str[s->length] = c;
  s->length++;
  s->str[s->length] = '\0';
  return STR_SUCCESS;
}

/**
*  Prida retezec "c" na konec retezce "s1"
* @param  *s  Ukazatel na strukturu
* @param  *c  Ukazatel na pridavany retezec
* @return  0  Pri uspechu
* @return  1  Pri neuspesne realokaci
*/
int strCatChar(string *s, char *c){
  for(int i = 0; i < strlen(c); i++){
    if(strCat(s, c[i]) == STR_ERROR)
      return STR_ERROR;
  }

  return STR_SUCCESS;
}

/**
* Prida retezec "s2" na konec retezce "s1"
* @param  *s1  Ukazatel na strukturu
* @param  *s2  Ukazatel na strukturu
* @return  0  Pri uspechu
* @return  1  Pri neuspesne realokaci
*/
strCatStr(string *s1, string *s2){
  if(strCatChar(s1, s2->str) == STR_ERROR)
    return STR_ERROR;

  return STR_SUCCESS;
}

/**
* Zkopiruje retezec v "s2" do "s1"
* @param  *s1  Ukazatel na strukturu
* @param  *s2  Ukazatel na strukturu
* @return  0   Pri uspechu
* @return  1   Pri neuspesne realokaci
*/
int  strCpy(string *s1, string *s2){

  while(s2->length >= s1->allocSize){ // nedostatek alokovane pameti
    if((s1->str = (char*) realloc(s1->str, s1->allocSize + STR_BLOCK)) == NULL)
      return STR_ERROR;
    s1->allocSize = s1->allocSize + STR_BLOCK;
  }

  strcpy(s1->str, s2->str);
  s1->length = s2->length;
  return STR_SUCCESS;
}

/**
* Porovna retezce v "s1" a "s2"
* @param  *s1  Ukazatel na strukturu
* @param  *s2  Ukazatel na strukturu
* @return  0   Pokud (s1 == s2)
*/
int  strCmp(string *s1, string *s2){
  return strcmp(s1->str, s2->str);
}

/**
* Porovna retezec v "s1" s retezcem "s2"
* @param  *s1  Ukazatel na strukturu
* @param  *s2  Ukazatel na retezec
* @return  0   Pokud (s1 == s2)
*/
int  strCmpConst(string *s1, char *s2){
  return strcmp(s1->str, s2);
}

/**
* Vrati textovou cast struktury
* @param   *s     Ukazatel na strukturu
* @return  *char  Textova cast struktury
*/
char *strGet(string *s){
  return s->str;
}

/**
* Vrati delku retezce
* @param  *s    Ukazatel na strukturu
* @return  int  Delka retezce
*/
int strLen(string *s){
  return s->length;
}

/**
* Vrati velikost alokovaneho prostoru
* @param  *s    Ukazatel na strukturu
* @return  int  Velikost alokovaneho prostoru
*/
int strSize(string *s){
  return s->allocSize;
}
